import numpy as np
from scipy.linalg import expm

import pyglasma3d.cy.solver_leapfrog as leapfrog
import pyglasma3d.cy.gauge as gauge
import pyglasma3d.cy.lattice as lattice

import pyglasma3d.core as core
import pyglasma3d.cy.gauss as gauss

"""
    Setup test simulation object
"""
nx, ny, nz = 8, 12, 7
dims = [nx, ny, nz]
iter_dims = [0, nx, 0, ny, 0, nz]
spacings = [0.1, 1.0, 1.0]
gc = 2.0
s = core.Simulation(dims, iter_dims, spacings, spacings[0] / 4.0, gc)
s.init()

"""
    Grid function tests (indices, shifting, ...)
"""


def test_index():
    indices = np.array(range(nx * ny * nz)).reshape((nx, ny, nz))
    indices_lattice = np.zeros((nx, ny, nz), dtype=np.int64)
    for ix in range(nx):
        for iy in range(ny):
            for iz in range(nz):
                pos = np.array([ix, iy, iz], dtype=np.int64)
                indices_lattice[ix, iy, iz] = lattice.get_index_p(pos, s.dims)
    assert all(np.equal(indices, indices_lattice).flatten())

    # test some random points back and forth
    for it in range(100):
        pos = np.array([np.random.randint(0, s.dims[d]) for d in range(3)], dtype=np.int64)
        x = lattice.get_index_p(pos, s.dims)
        posl = lattice.get_point_p(x, s.dims)

        assert np.array_equal(pos, posl)


def test_shift():
    pos = np.array([0, 0, 0], dtype=np.int64)
    index = lattice.get_index_p(pos, s.dims)
    assert index == 0

    # boundary tests
    assert lattice.shift_p(index, 0, 1, s.dims) == ny * nz
    assert lattice.shift_p(index, 1, 1, s.dims) == nz
    assert lattice.shift_p(index, 2, 1, s.dims) == 1

    assert lattice.shift_p(index, 0, -1, s.dims) == (nx - 1) * ny * nz
    assert lattice.shift_p(index, 1, -1, s.dims) == (ny - 1) * nz
    assert lattice.shift_p(index, 2, -1, s.dims) == (nz - 1)

    # random positions within box
    for i in range(100):
        pos = np.array([
            np.random.randint(1, nx - 1),
            np.random.randint(1, ny - 1),
            np.random.randint(1, nz - 1)
        ], dtype=np.int64)
        index = lattice.get_index_p(pos, s.dims)

        direction = np.random.randint(0, 3)
        shifted_index = lattice.shift_p(index, direction, 1, s.dims)
        shifted_pos = lattice.get_point_p(shifted_index, s.dims)
        assert shifted_pos[direction] == pos[direction] + 1
        shifted_index = lattice.shift_p(index, direction, -1, s.dims)
        shifted_pos = lattice.get_point_p(shifted_index, s.dims)
        assert shifted_pos[direction] == pos[direction] - 1


def test_shift2():
    pos = np.array([0, 0, 0], dtype=np.int64)
    index = lattice.get_index_p(pos, s.dims)
    assert index == 0

    assert lattice.shift2_p(index, 0, 1, s.dims, s.acc) == ny * nz
    assert lattice.shift2_p(index, 1, 1, s.dims, s.acc) == nz
    assert lattice.shift2_p(index, 2, 1, s.dims, s.acc) == 1

    assert lattice.shift2_p(index, 0, -1, s.dims, s.acc) == (nx - 1) * ny * nz
    assert lattice.shift2_p(index, 1, -1, s.dims, s.acc) == (ny - 1) * nz
    assert lattice.shift2_p(index, 2, -1, s.dims, s.acc) == (nz - 1)

    # random positions within box
    for i in range(100):
        pos = np.array([
            np.random.randint(1, nx - 1),
            np.random.randint(1, ny - 1),
            np.random.randint(1, nz - 1)
        ], dtype=np.int64)
        index = lattice.get_index_p(pos, s.dims)

        direction = np.random.randint(0, 3)
        shifted_index = lattice.shift2_p(index, direction, 1, s.dims, s.acc)
        shifted_pos = lattice.get_point_p(shifted_index, s.dims)
        assert shifted_pos[direction] == pos[direction] + 1
        shifted_index = lattice.shift2_p(index, direction, -1, s.dims, s.acc)
        shifted_pos = lattice.get_point_p(shifted_index, s.dims)
        assert shifted_pos[direction] == pos[direction] - 1


"""
    SU(2) group and algebra unit tests: checking against numpy matrix operations
"""

pauli = np.array([[[0, 1], [1, 0]], [[0, -1j], [1j, 0]], [[1, 0], [0, -1]]])


# some useful functions for converting the su2 parametrization into matrices and back


def su2_random_algebra():
    return 3.0 * (np.random.rand(3) - 0.5)


def su2_random_group():
    a = su2_random_algebra()
    ma = expm(su2_algebra(a) * 1.0j)
    comp = comps_group(ma)
    return a, ma, comp


def su2_algebra(a):
    result = np.zeros((2, 2), dtype=np.complex)
    for i in range(3):
        result += 0.5 * a[i] * pauli[i]
    return result


def su2_group(u):
    result = np.zeros((2, 2), dtype=np.complex)
    result += u[0] * np.identity(2)
    for i in range(3):
        result += u[i + 1] * pauli[i]
    return result


def comps_algebra(ma):
    result = np.zeros(3)
    result[:] = np.real(np.trace(ma.dot(pauli[:])))
    return result


def comps_group(mu):
    result = np.zeros(4)
    result[0] = np.real(0.5 * np.trace(mu))
    for i in range(3):
        result[i + 1] = np.imag(0.5 * np.trace(mu.dot(pauli[i])))
    return result


def proj(mu):
    result = np.zeros(3)
    for i in range(3):
        result[i] = np.imag(np.trace(mu.dot(pauli[i])))
    return result


def test_su2_multiplication():
    # multiplication of 2 random su(2) matrices
    a_a, a_m, a_c = su2_random_group()
    b_a, b_m, b_c = su2_random_group()
    r1 = comps_group(a_m.dot(b_m))
    r2 = comps_group(a_m.conj().T.dot(b_m))
    r3 = comps_group(a_m.dot(b_m.conj().T))
    r4 = comps_group(a_m.conj().T.dot(b_m.conj().T))

    lr1 = lattice.mul2_p(a_c, b_c, 1, 1)
    lr1_fast = lattice.mul2_fast_p(a_c, b_c)
    lr2 = lattice.mul2_p(a_c, b_c, -1, 1)
    lr3 = lattice.mul2_p(a_c, b_c, 1, -1)
    lr4 = lattice.mul2_p(a_c, b_c, -1, -1)

    assert all(np.isclose(r1, lr1))
    assert all(np.isclose(r1, lr1_fast))
    assert all(np.isclose(r2, lr2))
    assert all(np.isclose(r3, lr3))
    assert all(np.isclose(r4, lr4))

    # multiplication of 4 random matrices
    c_a, c_m, c_c = su2_random_group()
    d_a, d_m, d_c = su2_random_group()

    r1 = comps_group(a_m.dot(b_m).dot(c_m.conj().T).dot(d_m.conj().T))
    r2 = comps_group(a_m.dot(b_m.conj().T).dot(c_m.conj().T).dot(d_m))
    lr1 = lattice.mul4_p(a_c, b_c, c_c, d_c, 1, 1, -1, -1)
    lr2 = lattice.mul4_p(a_c, b_c, c_c, d_c, 1, -1, -1, 1)

    assert all(np.isclose(r1, lr1))
    assert all(np.isclose(r2, lr2))


def test_su2_mexp_proj():
    a_a, a_m, a_c = su2_random_group()
    r_exp = comps_group(a_m)
    lr_exp = lattice.mexp_p(a_a, 1.0)

    r_proj = proj(a_m)
    lr_proj = lattice.proj_p(comps_group(a_m))

    assert np.allclose(r_exp, lr_exp)
    assert np.allclose(r_proj, lr_proj)

    # special case: zero field exponentiated
    lr_exp = lattice.mexp_p(a_a, 0.0)
    assert np.allclose(lr_exp, [1.0, 0.0, 0.0, 0.0])


"""
    Plaquette functions
"""


def test_plaquettes():
    # test a few random positions and directions
    for it in range(10):
        # setup a plaquette to calculate
        randoms = np.arange(3)
        np.random.shuffle(randoms)
        di = randoms[0]
        dj = randoms[1]

        di_v = np.eye(1, 3, di)[0]
        dj_v = np.eye(1, 3, dj)[0]

        a_a, a_m, a_c = su2_random_group()
        b_a, b_m, b_c = su2_random_group()
        c_a, c_m, c_c = su2_random_group()
        d_a, d_m, d_c = su2_random_group()

        # 'positive' plaquette

        plaq = comps_group(a_m.dot(b_m).dot(c_m.conj().T).dot(d_m.conj().T))
        # plaq = comps_group(np.linalg.multi_dot(a_m, b_m, c_m.conj().T, d_m.conj().T))

        start_pos = np.zeros(3, dtype=np.int64)
        for i in range(3):
            start_pos[i] = np.random.randint(0, dims[i])

        pos0 = np.array(start_pos, np.int64)
        pos1 = np.array(start_pos + di_v, np.int64)
        pos2 = np.array(start_pos + dj_v, np.int64)
        pos3 = np.array(start_pos, np.int64)

        x0 = lattice.get_index_p(pos0, s.dims)
        x1 = lattice.get_index_p(pos1, s.dims)
        x2 = lattice.get_index_p(pos2, s.dims)
        x3 = lattice.get_index_p(pos3, s.dims)

        gi0 = 4 * (3 * x0 + di)
        gi1 = 4 * (3 * x1 + dj)
        gi2 = 4 * (3 * x2 + di)
        gi3 = 4 * (3 * x3 + dj)

        # reset fields in simulation object
        s.reset()

        s.u0[gi0:gi0 + 4:1] = a_c
        s.u0[gi1:gi1 + 4:1] = b_c
        s.u0[gi2:gi2 + 4:1] = c_c
        s.u0[gi3:gi3 + 4:1] = d_c

        lplaq = lattice.plaq_pos_p(s.u0, x0, di, dj, s.dims, s.acc)

        assert all(np.isclose(plaq, lplaq))

        # 'negative' plaquette

        plaq = comps_group(a_m.dot(b_m.conj().T).dot(c_m.conj().T).dot(d_m))

        pos0 = np.array(start_pos, np.int64)
        pos1 = np.array(start_pos + di_v - dj_v, np.int64)
        pos2 = np.array(start_pos - dj_v, np.int64)
        pos3 = np.array(start_pos - dj_v, np.int64)

        x0 = lattice.get_index_p(pos0, s.dims)
        x1 = lattice.get_index_p(pos1, s.dims)
        x2 = lattice.get_index_p(pos2, s.dims)
        x3 = lattice.get_index_p(pos3, s.dims)

        gi0 = 4 * (3 * x0 + di)
        gi1 = 4 * (3 * x1 + dj)
        gi2 = 4 * (3 * x2 + di)
        gi3 = 4 * (3 * x3 + dj)

        # reset fields in simulation object
        s.reset()

        s.u0[gi0:gi0 + 4:1] = a_c
        s.u0[gi1:gi1 + 4:1] = b_c
        s.u0[gi2:gi2 + 4:1] = c_c
        s.u0[gi3:gi3 + 4:1] = d_c

        lplaq = lattice.plaq_neg_p(s.u0, x0, di, dj, s.dims, s.acc)

        assert all(np.isclose(plaq, lplaq))


"""
    Gauge link update tests
"""


def test_gauge_link_update():
    # reset fields in simulation object
    s.reset()

    # set random electric field
    s.e = (np.random.rand(3 * 3 * s.N) - 0.5) * 0.5

    # compute what gauge links should look like
    dt = s.dt
    u_r = np.empty(4 * 3 * s.N, dtype=np.double)
    for i in range(3 * s.N):
        r = expm(- 1.0j * dt * su2_algebra(s.e[3 * i: 3 * (i + 1)]))
        u_r[4 * i: 4 * (i + 1)] = comps_group(r)

    # evolve gauge links
    leapfrog.evolve_u(s)

    assert np.allclose(u_r, s.u1)


def test_inv_gauge_link_update():
    # reset fields in simulation object
    s.reset()

    # set random electric field
    s.e = (np.random.rand(3 * 3 * s.N) - 0.5) * 0.1

    # evolve
    leapfrog.evolve_u(s)
    e = s.e.copy()

    # do inverse gauge link update
    leapfrog.evolve_u_inv(s)

    # compare electric field
    assert np.allclose(e, s.e)


"""
    Gauss constraint test
"""


def test_gauss_constraint():
    for it in range(10):
        # reset fields
        s.reset()

        # setup specific electric field
        # E^a_{x,i} = A_i B^a (n_j x_j)
        # (\partial_i E^a_{x,i})^2 = (A_i n_i)^2 (B^a)^2.
        amplitude = 1.0
        n = np.random.uniform(-0.5, 0.5, 3)
        n /= np.linalg.norm(n)
        A = np.random.uniform(-amplitude, amplitude, 3)
        B = np.array(np.random.uniform(-amplitude, amplitude, 3), np.double)
        unit = np.zeros(3, dtype=np.double)
        unit[:] = s.g * s.a[:]

        for ix in range(s.dims[0]):
            for iy in range(s.dims[1]):
                for iz in range(s.dims[2]):
                    pos = np.array([ix, iy, iz], np.int64)
                    x = lattice.get_index_p(pos, s.dims)
                    pos2 = lattice.get_point_p(x, s.dims)
                    assert np.array_equal(pos, pos2)
                    dot = s.a[0] * pos[0] * n[0] + s.a[1] * pos[1] * n[1] + s.a[2] * pos[2] * n[2]
                    for d in range(3):
                        field_index = 3 * (3 * x + d)
                        s.e[field_index:field_index + 3] = A[d] * B[0:3] * dot * unit[d]

        # check against analytical result
        gauss_analytic = np.dot(A, n) ** 2 * np.dot(B, B)
        for it in range(100):
            pos = np.array([np.random.randint(1, s.dims[d] - 1) for d in range(3)], dtype=np.int64)
            x = lattice.get_index_p(pos, s.dims)
            gauss_lattice = gauss.gauss_sq(s, x)
            assert np.isclose(gauss_analytic, gauss_lattice)

        # apply random gauge
        g = np.zeros(4 * nx * ny * nz, dtype=np.double)
        for x in range(nx * ny * nz):
            _, _, rg = su2_random_group()
            g[4 * x:4 * (x + 1)] = rg
        gauge.apply_gauge(s, g)

        # check against analytical result
        for it in range(100):
            pos = np.array([np.random.randint(1, s.dims[d] - 1) for d in range(3)], dtype=np.int64)
            x = lattice.get_index_p(pos, s.dims)
            gauss_lattice = gauss.gauss_sq(s, x)
            assert np.isclose(gauss_analytic, gauss_lattice)

        # evolve for some time
        for t in range(100):
            s.evolve()

        # check against analytical result
        for it in range(100):
            pos = np.array([np.random.randint(1, s.dims[d] - 1) for d in range(3)], dtype=np.int64)
            x = lattice.get_index_p(pos, s.dims)
            gauss_lattice = gauss.gauss_sq(s, x)
            assert np.isclose(gauss_analytic, gauss_lattice)