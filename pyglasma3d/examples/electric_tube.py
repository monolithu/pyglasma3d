#!/usr/bin/python

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import pyglasma3d.cy.energy as energy
from pyglasma3d.core import SimulationImplicit as Simulation


# simulation parameters
nx, ny, nz = 128, 64, 1
dims = [nx, ny, nz]
iter_dims = [0, nx, 0, ny, 0, nz]
a = [0.5, 1.0, 1.0]
dt = a[0]
g = 2.0

# start of simulation
s1 = Simulation(dims, iter_dims, a, dt, g)
s1.iterations = 10
s1.damping = 0.65
s1.mode = 4


def init(s):
    # initial conditions, a tube of electric field in e_z
    amp = 1
    n1, n2, n3 = nx, ny, nz
    i1, i2, i3 = 0, 1, 2
    m = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]).T

    c = 1
    di = 2

    for p1 in xrange(n1):
        for p2 in xrange(n2):
            d1, d2 = a[i1] * (p1 - n1 / 2.0), a[i2] * (p2 - 1 * n2 / 4.0)
            d = np.sqrt(d1 ** 2 + d2 ** 2) / np.sqrt((a[i1] * n1 / 2) ** 2 + (a[i2] * n2 / 2) ** 2)
            for p3 in xrange(n3):
                ix, iy, iz = np.dot(m, [p1, p2, p3])
                index = nz * (ny * ix + iy) + iz
                i = 3 * (3 * index + di) + c
                field = np.exp(- 512.0 * d ** 2)
                s.e[i] += field * amp

    c = 2
    di = 2

    for p1 in xrange(n1):
        for p2 in xrange(n2):
            d1, d2 = a[i1] * (p1 - n1 / 2.0), a[i2] * (p2 - 3 * n2 / 4.0)
            d = np.sqrt(d1 ** 2 + d2 ** 2) / np.sqrt((a[i1] * n1 / 2) ** 2 + (a[i2] * n2 / 2) ** 2)
            for p3 in xrange(n3):
                ix, iy, iz = np.dot(m, [p1, p2, p3])
                index = nz * (ny * ix + iy) + iz
                i = 3 * (3 * index + di) + c
                field = np.exp(- 512.0 * d ** 2)
                s.e[i] += field * amp

    s.init()

init(s1)

plt.ion()

for step in range(100):
    s1.evolve_initial()
    s1.evolve_iterate()

    plt.clf()
    e1 = energy.energy_3d(s1)[:, :, 0]
    e1 /= np.sum(e1)
    plt.imshow(e1, aspect=a[0], interpolation='none')
    plt.pause(0.000001)

    if step % 20 == 0:
        print(s1.gauss_constraint())
