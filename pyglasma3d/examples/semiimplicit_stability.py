#!/usr/bin/python
from __future__ import print_function
import numpy as np

import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import SimulationImplicit as Simulation
import pyglasma3d.core


# simulation parameters
nx, ny, nz = 128, 16, 16
dims = [nx, ny, nz]
iter_dims = [1, nx-1, 0, ny, 0, nz]
a = [0.5, 1.0, 1.0]
dt = a[0]
g = 2.0
sigma = 1*a[0]

iters_array = [5, 10, 15, 20]
alpha_array = np.arange(0.0, 0.8, 0.05)


for citer in iters_array:
    for alpha in alpha_array:
        # simulation object
        np.random.seed(1)
        s = Simulation(dims, iter_dims, a, dt, g)
        s.mode = 1
        s.debug = True
        s.iterations = citer
        s.damping = alpha
        s.reset()

        # init
        v1 = mv.initialize_mv(s, x0=dims[0] * 0.25 * a[0], mu=0.2, sigma=sigma, mass=0.1, uvt=1.0, orientation=+1)
        interpolate.initialize_charge_planes(s, +1, 1, dims[0] / 2)
        v2 = mv.initialize_mv(s, x0=dims[0] * 0.75 * a[0], mu=0.2, sigma=sigma, mass=0.1, uvt=1.0, orientation=-1)
        interpolate.initialize_charge_planes(s, -1, dims[0] / 2, dims[0] - 1)
        interpolate.interpolate_to_charge_density(s)
        s.init()

        # evolve
        for it in range(nx/2):
            s.evolve()

        # measure
        print(citer, alpha, pyglasma3d.core.semiimplicit.gauss_constraint_violation(s))