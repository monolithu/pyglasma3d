#!/usr/bin/python

from __future__ import print_function

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as colors

import numpy as np
import sys
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.coulomb as coulomb
import pyglasma3d.cy.gluonspectrum as gluonspectrum
import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import Simulation

# simulation parameters
nx, ny, nz = 128, 64, 64
dims = [nx, ny, nz]
iter_dims = [1, nx-1, 0, ny, 0, nz]
a = [1.0, 1.0, 1.0]
dt = 0.5
g = 2.0

# start of simulation
s = Simulation(dims, iter_dims, a, dt, g)
o = 1
num_events = 1

np.random.seed(2323)

# simple plotting of energy density and occupation numbers
def show(s):
    occ = gluonspectrum.compute_occupation_numbers1(s)
    occ = np.fft.fftshift(occ)
    mx = np.max(occ)
    print(np.sum(occ))

    """
    X, Y = np.meshgrid(np.linspace(0, 1, ny), np.linspace(0, 1, nz))
    fig = plt.figure()
    plot = fig.add_subplot(111, projection="3d")
    num = 5
    for i in range(num+1):
        off = i / (1.0 * num)
        plot.contourf(X, Y, occ[min(int(off*nx), nx-1)], zdir="z", offset=off, alpha=0.3,
                      vmin=mx * 0.00001, vmax=mx)
    plot.set_xlim(0, 1)
    plot.set_ylim(0, 1)
    plot.set_zlim(0, 1)
    fig.show()
    """

    plt.imshow(occ[nx/2])
    plt.show()

# initial conditions
s.reset()
mv.initialize_mv(s, x0=dims[0] * 0.25, mu=0.1, sigma=4.0, mass=0.1, uvt=np.pi/2, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
mv.initialize_mv(s, x0=dims[0] * 0.75, mu=0.1, sigma=4.0, mass=0.1, uvt=np.pi/2, orientation=-1)
interpolate.initialize_charge_planes(s, -1, nx/2, nx)
s.init()
s.evolve()

show(s)

# evolve until collision event
for it in range(nx/2):
    s.evolve()
show(s)

# evolve some more
for it in range(nx/2):
    s.evolve()
show(s)

plt.show()