#!/usr/bin/python

from __future__ import print_function

from datetime import datetime
import matplotlib.pyplot as plt
import numpy
import numpy as np
import sys
import os
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import SimulationImplicit
import pyglasma3d.core
from pyglasma3d.core import Simulation

"""
    A script to compare different solvers with regards to the numerical Cherenkov instability.

    We look at the propagation of a single nucleus given by the MV model. In the continuum such a field configuration
    should stay static while moving at the speed of light. On the lattice, due to numerical dispersion, the field
    becomes unstable and leads to an unphysical exponential energy increase.

    We look at the performance of different numerical schemes for lattice Yang-Mills, in particular the standard
    leapfrog solver and our new semi-implicit with regards to this numerical instability.
"""
# parse arguments
# format: python script.py <solver mode> <solver iterations> <solver alpha> <long. lattice size> <output file>
if len(sys.argv) != 1+5:
    print("Malformed arguments! What are you doing?")
    exit()

# available modes: 'leapfrog', 'implicit', 'semi-implicit'
mode = str(sys.argv[1])
iterations = int(sys.argv[2])
damping = float(sys.argv[3])
n_l = int(sys.argv[4])
otp = str(sys.argv[5])

if mode == 'leapfrog':
    step = 2
else:
    step = 1

"""
    Initial conditions parameters
"""

# Lattice size
nx, ny, nz = n_l, 128, 128

# Transverse and longitudinal box widths [fm]
LT = 4.0
LL = 4.0

# Collision energy [MeV]
sqrts = 200.0 * 1000.0

# Infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 20.0 * 1000.0

"""
    The rest of the parameters are computed automatically.
"""

# Constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# Determine lattice spacings and energy units
aT_fm = LT / ny
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = LL / nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL/step

# Determine initial condition parameters in lattice units
gamma = sqrts / 2000.0
Qs = np.sqrt((sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = uv / E0
ir = m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = nx / 2
nx += 2

# output file
file_path = "./output/" + otp

numpy.random.seed(1)
DEBUG = True

dims = [nx, ny, nz]
iter_dims = [1, nx-1, 0, ny, 0, nz]

s = None
if mode == 'leapfrog':
    s = Simulation(dims, iter_dims, a, dt, g)
elif mode == 'implicit':
    s = SimulationImplicit(dims, iter_dims, a, dt, g)
    s.mode = 0
    s.damping = damping
    s.iterations = iterations
elif mode == 'semi-implicit':
    s = SimulationImplicit(dims, iter_dims, a, dt, g)
    s.mode = 1
    s.damping = damping
    s.iterations = iterations
else:
    print("Unknown solver mode. What are you doign???")
    exit()
s.debug = DEBUG

s.log("Starting simulation at", datetime.now())
s.log("Memory use:", int(s.get_nbytes() / (1024 * 1024)), "mb")

t = time.time()
s.log("Initializing left nucleus.")
v = mv.initialize_mv_inc(s=s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
s.log("Initialized left nucleus.", round(time.time() - t, 3), "s")
s.init()
s.log("Memory use:", int(s.get_nbytes() / (1024 * 1024)), "mb")

# loop
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for st in range(step):
        t = time.time()
        s.evolve()
        s.log("Complete cycle.", round(time.time() - t, 3), "s")
        t = time.time()
    s.write_energy_momentum(max_iters, file_path)
    s.log("Writing energy momentum tensor to file.", round(time.time() - t, 3), "s")

s.log("Completed simulation at", datetime.now())

exit()
