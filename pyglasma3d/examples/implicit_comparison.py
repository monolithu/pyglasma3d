#!/usr/bin/python

from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np
import sys
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import SimulationImplicit
from pyglasma3d.core import Simulation
import pyglasma3d

# simulation parameters
nx, ny, nz = 64, 8, 8
dims = [nx, ny, nz]
iter_dims = [2, nx-2, 0, ny, 0, nz]
a = [0.125, 1.0, 1.0]
dt = a[0]
g = 2.0
sigma = 0.25
mu = 1.0


# start of simulation
s_e = Simulation(dims, iter_dims, a, dt / 2, g)
s_i = SimulationImplicit(dims, iter_dims, a, dt, g)
s_i.mode = 4
s_i.iterations = 16
s_i.damping = 0.0


# init simulation
def init(s):
    np.random.seed(10)
    mv.initialize_mv(s, x0=dims[0] * 0.25 * a[0], mu=mu, sigma=sigma, mass=mu/3, uvt=2.0, orientation=+1)
    interpolate.initialize_charge_planes(s, +1, 1, dims[0] / 2)
    #mv.initialize_mv(s, x0=dims[0] * 0.75 * a[0], mu=mu, sigma=sigma, mass=mu/3, uvt=2.0, orientation=-1)
    #interpolate.initialize_charge_planes(s, -1, dims[0] / 2, dims[0] - 1)
    s.init()

init(s_e)
init(s_i)

s_e.evolve()

# normalization for the plot
EL_e, BL_e, ET_e, BT_e, SL_e = Observables.energy_momentum_tensor(s_e)
EL_i, BL_i, ET_i, BT_i, SL_i = Observables.energy_momentum_tensor(s_i)
max_ee = np.sum(EL_e + BL_e + ET_e + BT_e)
max_ei = np.sum(EL_i + BL_i + ET_i + BT_i)

# energy
diff_e = []
diff_i = []
total_e0 = 0
total_i0 = 0

plt.ion()
iters = int(nx*3/4 * 0.8)
for it in range(iters):
    s_i.evolve()
    s_e.evolve()
    s_e.evolve()

    EL_e, BL_e, ET_e, BT_e, SL_e = Observables.energy_momentum_tensor(s_e)
    EL_i, BL_i, ET_i, BT_i, SL_i = Observables.energy_momentum_tensor(s_i)

    plt.figure(0)
    plt.clf()
    plt.title("Field components")
    plt.axis([0, dims[0], 0, 1])
    plt.autoscale(True, axis='y')
    xaxis = np.arange(1, dims[0] - 1)

    plt.plot(xaxis, EL_e/max_ee, c='red', label='EL')
    plt.plot(xaxis, BL_e/max_ee, c='orange', label='BL')
    plt.plot(xaxis, ET_e/max_ee, c='blue', label='ET')
    plt.plot(xaxis, BT_e/max_ee, c='green', label='BT')

    plt.plot(xaxis, EL_i/max_ei, c='red', linestyle='--')
    plt.plot(xaxis, BL_i/max_ei, c='orange', linestyle='--')
    plt.plot(xaxis, ET_i/max_ei, c='blue', linestyle='--')
    plt.plot(xaxis, BT_i/max_ei, c='green', linestyle='--')

    legend = plt.legend(loc='upper right', shadow=True)

#    v_i, de_i, r_i = gauss.rel_gauss_violation(s_i)
#    v_e, de_e, r_e = gauss.rel_gauss_violation(s_e)
    #print(v_i, de_i, r_i)
    if s_i.mode == 1:
        print(pyglasma3d.core.semiimplicit.gauss_constraint_violation(s_i))
    elif s_i.mode == 4:
        print(pyglasma3d.core.semiimplicit2.gauss_constraint_violation(s_i))

    # inset for time derivative of energy
    total_e1 = np.sum(EL_e + BL_e + ET_e + BT_e)
    total_i1 = np.sum(EL_i + BL_i + ET_i + BT_i)
    if it > 0:
        plt.figure(1)
        plt.clf()
        plt.title("(E(t) - E(0)) / E(0)")
        diff_e.append((total_e1-max_ee)/max_ee)
        diff_i.append((total_i1-max_ei)/max_ei)
        plt.plot(diff_e, c='black')
        plt.plot(diff_i, c='black', linestyle='--')
        plt.show()
    total_e0 = total_e1
    total_i0 = total_i1

    plt.figure(0)
    plt.pause(0.000000001)
