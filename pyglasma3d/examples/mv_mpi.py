from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.interpolate as interpolate
import pyglasma3d.cy.mv as mv

from pyglasma3d.cy.mpi import MPISimulation

"""
    A script for Au-Au collisions in the MV-model similar to what we did in our last publication.
    The main input parameters are the collision energy \sqrt{s}, the infrared regulator m and the
    box dimensions (LT, LL).
"""

# start with
# `mpiexec -n 3 python example_mpi2.py` (locally)

run = 'mv_mpi_trial'    # data will end up in `./output/T_<run>.dat`

# node configuration (important for correct setup of initial conditions)
node_config = {
    'num': 3,       # intended number of nodes
    'left': 0,      # node with right moving nucleus (i.e. the left nucleus)
    'right': 2      # node with left moving nucleus (i.e. the right nucleus)
}

# grid size (make sure that ny == nz)
nx, ny, nz = 1024, 256, 256

# transverse and longitudinal box widths [fm]
LT = 6.0
LL = 6.0

# collision energy [MeV]
sqrts = 200.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 10.0 * 1000.0

# ratio between dt and aL [int, multiple of 2]
steps = 4

# option for mpi debug information
mpi_debug = False

"""
    The rest of the parameters are computed automatically.
"""

# constants
hbarc   = 197.3270      # hbarc [MeV*fm]
RAu     = 7.27331       # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm   = LT / ny
E0      = hbarc / aT_fm
aT      = 1.0
aL_fm   = LL / (node_config['num'] * nx)
aL      = aL_fm / aT_fm
a       = [aL, aT, aT]
dt      = aL / steps

# determine initial condition parameters
gamma   = sqrts / 2000.0
Qs      = np.sqrt((sqrts/1000.0) ** 0.25) * 1000.0
alphas  = 12.5664 / (18.0 * np.log(Qs / 217.0))
g       = np.sqrt(12.5664 * alphas)
mu      = Qs / (g * g * 0.75) / E0
uvt     = uv / E0
ir      = m / E0
sigma   = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = (node_config['num']-1) * nx
file_path = './output/T_' + run + '.dat'

"""
    Initialization
"""

# initialize simulation object
dims = [nx, ny, nz]
mpi_s = MPISimulation(dimensions=dims, spacings=a, time_step=dt, coupling=g, node_config=node_config)
mpi_s.debug = mpi_debug
s = mpi_s.s

if mpi_s.rank == node_config['left']:
    v = mv.initialize_mv(s, x0=nx * 0.5 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
    interpolate.initialize_charge_planes(s, +1, 2, nx - 2)
    s.init()

if mpi_s.rank == node_config['right']:
    v = mv.initialize_mv(s, x0=nx * 0.5 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1)
    interpolate.initialize_charge_planes(s, -1, 2, nx - 2)
    s.init()

# synchronize nodes with correct boundary conditions
mpi_s.init()

"""
    Simulation loop
"""

t = time.time()
for it in range(max_iters):
    # this for loop moves the nuclei exactly one grid cell
    for step in range(steps):
        t = time.time()
        mpi_s.evolve()
        print(mpi_s.rank, mpi_s.s.t, "Complete cycle", round(time.time()-t, 3))

    t = time.time()
    mpi_s.write_energy_momentum(max_iters, file_path)
    print(mpi_s.rank, mpi_s.s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))
