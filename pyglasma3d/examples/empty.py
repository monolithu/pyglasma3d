#!/usr/bin/python

from __future__ import print_function
import matplotlib.pyplot as plt
import numpy
import time
from pyglasma3d.core import Simulation, Observables

# simulation parameters
nx, ny, nz = 64, 64, 64
dims = [nx, ny, nz]
iter_dims = [0, nx, 0, ny, 0, nz]
a = [1.0, 1.0, 1.0]
dt = 0.5
g = 1.0

DEBUG = True

# start of simulation
print("creating simulation object")
s = Simulation(dims, iter_dims, a, dt, g)
s.debug = DEBUG
print("size of simulation object:", round(s.get_nbytes() / 1024.0 ** 3, 4), "gb")

# run before starting evolution, after initial conditions have been applied
s.init()

# initialize interactive plot
plt.ion()
t = time.time()
iters = 1000
times = []

total_time = time.time()

for it in range(iters):
    # evolve and measure
    t = time.time()
    s.evolve()
    GS = Observables.gauss_constraint_squared(s)
    EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(s)
    times.append(time.time() - t)
    print("iteration " + str(it) + "/" + str(iters) + " : " + str(round(time.time() - t, 3)) + " s")
    # compute projected energy densities

print("Avg time")
print(numpy.mean(times), "s")

print("Total time")
print(time.time() - total_time, "s")
