from __future__ import print_function

import argparse
import time
import numpy as np
import sys
import os
from datetime import datetime

"""
    "Ultimate" batch script for MV model collisions v1

    A convenience script that handles the configuration, queuing, running and processing of a simulation job.
    The main idea is to have one script corresponding to a job/batch for a specific set of parameters, i.e. one script
    for an average over 20 events with sqrts = 200 GeV plus some other parameters. This creates a lot of redundancy
    (and lots of copy/pasting of this script), but also makes it easier to replicate results even as the code ages.
    Working with our simulations I often forgot after a while what parameters I used for certain data sets. Using
    this script (or similar scripts) this should be less of a problem in the future.

    Usage:
    First, make a copy of this script and rename it to whatever you want. Example: mv_test.py
    Change the parameters as you like (collision energy, lattice and box size, MPI on/off, queue on the cluster, ..)

    On the login node of the cluster run

       `python mv_test.py batch`

    to create all Slurm scripts automatically and add them to the queue.

    After all jobs have finished run (either on the login node or in an interactive job)

        `python mv_test.py average`

    to average over all events and optionally downsample the data. You end up with a single file of averaged results
    all according to the configuration set in this script.

    Behind the scenes:
    Calling the `batch` mode creates the Slurm scripts and queues them up. The Slurm scripts execute this script with
    the `run` mode and the id of the event (0, 1, 2, ..., num_events-1), which runs the simulation code at the end of
    this script with the parameters given in this script. The `average` mode scans for the files created by this script
    and averages over all events. Optionally the data can be downsampled for easier transfer and handling.

"""

"""
    User settings
"""

# Job name and output directory. Output data will end up in
# output_folder + 'T_' + job_name + 0,1,2,3, etc
job_name = 'batchtest'
output_folder = './output/'

# Folder for Slurm scripts
script_folder = './slurm_scripts/'

# Folder for log files
log_folder = './logs/'

# Queue settings
# 'private'     -   private institute queue with 256gb nodes
# 'normal64'    -   normal queue with 64gb nodes
# 'normal128'   -   normal queue with 128gb nodes
# 'normal256'   -   normal queue with 256gb nodes
# 'devel'       -   developer queue with 128gb nodes, limited to 10min runtime
queue = 'devel'

# Number of events and offset for enumeration
num_events = 3
event_no_offset = 0

# Debug settings
use_debug = True

# MPI settings
MPI = True
MPI_num_nodes = 2
MPI_debug = True

# Lattice size
nx, ny, nz = 128, 64, 64

# Transverse and longitudinal box widths [fm] (all nodes together in case of MPI)
LT = 4.0
LL = 4.0

# Collision energy [MeV]
sqrts = 200.0 * 1000.0

# Infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 20.0 * 1000.0

# Longitudinal coherence length (ratio of coherence length to width)
# kappa ~ 0.1 corresponds to 'realistic' nuclei
kappa = 1.0

# Ratio between dt and aL [int, multiple of 2]
steps = 2

"""
    The rest of the parameters are computed automatically.
"""

if not MPI:
    # override number of nodes if not using MPI
    MPI_num_nodes = 1

# Constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# Determine lattice spacings and energy units
aT_fm = LT / ny
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = LL / (MPI_num_nodes * nx)
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL / steps

# Determine initial condition parameters in lattice units
gamma = sqrts / 2000.0
Qs = np.sqrt((sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = uv / E0
ir = m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL
clen = sigma * kappa
clen_c = clen / aL

# number of evolution steps (max_iters) and output file path
if MPI:
    if MPI_num_nodes % 2 == 0:
        # even number of nodes
        max_iters = MPI_num_nodes / 2 * nx
    else:
        # odd number of nodes
        max_iters = (MPI_num_nodes + 1) / 2 * nx
    # add 'ghost' regions if using MPI
    nx += 2
else:
    max_iters = nx / 2
    nx += 2

"""
    Argument parsing
"""

parser = argparse.ArgumentParser(description='MV collisions batch script')

subparsers = parser.add_subparsers(dest='mode')

subparser_batch = subparsers.add_parser('batch')
subparser_batch.add_argument('--dontsubmit', action='store_true')

subparser_run = subparsers.add_parser('run')
subparser_run.add_argument('--event', type=int)

subparser_average = subparsers.add_parser('average')
subparser_average.add_argument('--step', type=int, default=1)

args = parser.parse_args()

"""
    Batch mode
"""

if args.mode == 'batch':
    print("Running in batch mode")

    # create scripts directory
    if not os.path.isdir(script_folder):
        print("Slurm script folder does not exist. Trying to create..")
        os.makedirs(script_folder)

    # create log directory
    if not os.path.isdir(log_folder):
        print("Log folder does not exist. Trying to create..")
        os.makedirs(log_folder)

    # queues on the cluster
    queue_template = {
        'private': """
#SBATCH --partition=mem_0256
#SBATCH --qos=p70623_0256
#SBATCH --account=p70623
""",
        'normal64': """
#SBATCH --partition=mem_0064
#SBATCH --qos=normal_0064
""",
        'normal128': """
#SBATCH --partition=mem_0128
#SBATCH --qos=normal_0128
""",
        'normal256': """
#SBATCH --partition=mem_0256
#SBATCH --qos=normal_0256
""",
        'devel': """
#SBATCH --partition=mem_0128
#SBATCH --qos=devel_0128
""",
    }

    # Slurm script template
    slurm_template = """#!/bin/bash
#
#SBATCH -J {job_name}

#SBATCH -N {num_nodes}
#SBATCH --ntasks-per-node=1
{queue_string}
#SBATCH --output={batch_log_path}

export OMP_NUM_THREADS=32

export I_MPI_PMI_LIBRARY=/cm/shared/apps/slurm/current/lib/libpmi.so
export I_MPI_OFA_TRANSLATION_CACHE=on
export KMP_DUPLICATE_LIB_OK=TRUE

module purge
module load intel/16 intel-mpi/5.1 python/2.7 mpi4py/2.0.0 intel-mkl/11 numpy/1.12.0 likwid/4.0

srun -o {single_job_log_path} python {python_script_name} run --event {event_no}
"""

    # create all Slurm scripts
    event_nos = range(event_no_offset, num_events + event_no_offset)
    names = [job_name + "_e" + str(ev) for ev in event_nos]

    for event_no in event_nos:
        name = names[event_no - event_no_offset]

        batch_log_path = os.path.join(log_folder, name + '_%A_batch.log')

        if MPI:
            single_job_log_path = os.path.join(log_folder, name + '_%A_r%t.log')
            if MPI_num_nodes <= 1:
                print("Error: trying to use MPI with just a single node. Set MPI_num_nodes to something larger than 1.")
                sys.exit()
        else:
            single_job_log_path = os.path.join(log_folder, name + '_%A.log')

        python_script_name = os.path.basename(sys.argv[0])

        slurm_string = slurm_template.format(job_name=name,
                                             queue_string=queue_template[queue],
                                             num_nodes=MPI_num_nodes,
                                             batch_log_path=batch_log_path,
                                             single_job_log_path=single_job_log_path,
                                             python_script_name=python_script_name,
                                             event_no=event_no,
                                             )
        slurm_script_path = os.path.join(script_folder, name + '.slrm')

        f = open(slurm_script_path, "w")
        f.write(slurm_string)
        f.close()

        print("Created slurm script " + slurm_script_path)

    # submit slurm scripts
    if not args.dontsubmit:
        print("Submitting scripts ..")
        for name in names:
            slurm_script_path = os.path.join(script_folder, name + '.slrm')
            print("Submit " + slurm_script_path)
            os.system("sbatch " + slurm_script_path)

    sys.exit()

"""
    Run mode (simulation code)
"""

if args.mode == 'run':
    """
    Note: Importing modules here (instead of at the beginning of the script) isn't good style, but the login node of the
    cluster won't let me import MPI modules when this script is run in batch mode. The solution is to import MPI only
    when this script runs in 'run' mode, which should happen only on compute nodes anyways.
    """
    from pyglasma3d.cy.mpi import MPISimulation
    from pyglasma3d.core import Simulation
    import pyglasma3d.mv as mv
    import pyglasma3d.interpolate as interpolate

    event_no = args.event

    if MPI:
        # create node config
        if MPI_num_nodes % 2 == 0:
            # even number of nodes
            node_config = {
                'num': MPI_num_nodes,               # intended number of nodes
                'left': (MPI_num_nodes - 2) / 2,    # left nucleus
                'right': MPI_num_nodes / 2,         # right nucleus
            }
        else:
            # odd number of nodes
            node_config = {
                'num': MPI_num_nodes,               # intended number of nodes
                'left': (MPI_num_nodes - 3) / 2,    # left nucleus
                'right': (MPI_num_nodes + 1) / 2    # right nucleus
            }

        # initialize simulation object
        dims = [nx, ny, nz]
        mpi_s = MPISimulation(dimensions=dims, spacings=a, time_step=dt, coupling=g, node_config=node_config)
        mpi_s.debug = MPI_debug
        s = mpi_s.s
        s.debug = use_debug

        mpi_s.log("Single node run mode")
        mpi_s.log("Starting event #", event_no, "at", datetime.now())

        t = time.time()
        mpi_s.log("Initializing left nucleus.")
        if mpi_s.rank == node_config['left']:
            v = mv.initialize_mv_inc(s, nx * 0.5 * aL, mu, sigma, kappa, ir, uvt, +1)
            interpolate.initialize_charge_planes(s, +1, 2, nx - 2)
            s.init()
        mpi_s.log("Initialized left nucleus.", round(time.time()-t, 3), "s")

        t = time.time()
        mpi_s.log("Initializing right nucleus.")
        if mpi_s.rank == node_config['right']:
            v = mv.initialize_mv_inc(s, nx * 0.5 * aL, mu, sigma, kappa, ir, uvt, -1)
            interpolate.initialize_charge_planes(s, -1, 2, nx - 2)
            s.init()
            mpi_s.log("Initialized right nucleus.", round(time.time() - t, 3), "s")

        # synchronize nodes with correct boundary conditions
        mpi_s.init()

        # set output path
        file_path = os.path.join(output_folder, 'T_{0}_e{1}.dat'.format(job_name, event_no))

        # simulation loop

        t = time.time()
        for it in range(max_iters):
            # this for loop moves the nuclei exactly one grid cell
            for step in range(steps):
                t = time.time()
                mpi_s.evolve()
                mpi_s.log("Complete cycle", round(time.time() - t, 3), "s")

            t = time.time()
            mpi_s.write_energy_momentum(max_iters, file_path)
            mpi_s.log("Writing energy momentum tensor to file.", round(time.time() - t, 3), "s")

        mpi_s.log("Completed event #", event_no, "at", datetime.now())

    else:
        # initialize simulation object
        dims = [nx, ny, nz]
        s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx-1, 0, ny, 0, nz])
        s.debug = use_debug

        # mv initial conditions
        s.log("Single node run mode")
        s.log("Starting event #", event_no, "at", datetime.now())

        t = time.time()
        s.log("Initializing left nucleus.")
        v = mv.initialize_mv_inc(s, nx * 0.25 * aL, mu, sigma, kappa, ir, uvt, +1)
        interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
        s.log("Initialized left nucleus.", round(time.time()-t, 3), "s")

        t = time.time()
        s.log("Initializing right nucleus.")
        v = mv.initialize_mv_inc(s, nx * 0.75 * aL, mu, sigma, kappa, ir, uvt, -1)
        interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
        s.log("Initialized right nucleus.", round(time.time() - t, 3), "s")

        s.init()

        # set output path
        file_path = os.path.join(output_folder, 'T_{0}_e{1}.dat'.format(job_name, event_no))

        # simulation loop

        for it in range(max_iters):
            # this for loop moves the nuclei exactly one grid cell
            for step in range(steps):
                t = time.time()
                s.evolve()
                s.log("Complete cycle.", round(time.time()-t, 3), "s")

            t = time.time()

            s.write_energy_momentum(max_iters, file_path)
            s.log("Writing energy momentum tensor to file.", round(time.time()-t, 3), "s")

        s.log("Completed event #", event_no, "at", datetime.now())

    sys.exit()

# average mode: average (and downsample) data specific to this script

if args.mode == 'average':
    # for consistency I also import the necessary modules only here to de-clutter the top imports.
    from pyglasma3d.utils.downsample import downsample2
    from pyglasma3d.utils.data_io import write

    # input format
    input_path = os.path.join(output_folder, 'T_' + job_name + '_e')
    output_path = os.path.join(output_folder, 'T_' + job_name + '_avg.dat')
    step = int(args.step)

    # get files based on input path (i.e. starting with)
    parent = os.path.dirname(input_path)

    files = []
    for f in os.listdir(parent):
        if os.path.join(parent, f).startswith(input_path):
            files.append(os.path.join(parent, f))

    print("Averaging the following files with step " + str(step))
    for f in files:
        print(f)

    # downsample each file and average
    data = downsample2(files[0], step)
    for fi in range(1, len(files)):
        f = files[fi]
        data += downsample2(f, step)
    data = np.multiply(data, 1.0/len(files))
    write(output_path, data[0], data[1], data[2], data[3], data[4])

    print("Averaging complete.")

    sys.exit()
