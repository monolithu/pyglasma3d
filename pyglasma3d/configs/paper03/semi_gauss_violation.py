#!/usr/bin/python

from __future__ import print_function

from datetime import datetime
import numpy
import numpy as np
import sys
import os
import time

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.mv as mv

import pyglasma3d.cy.interpolate as interpolate
from pyglasma3d.core import Observables
from pyglasma3d.core import SimulationImplicit
import pyglasma3d.core
from pyglasma3d.core import Simulation

"""
    A script to compare different solvers with regards to the numerical Cherenkov instability.

    We look at the propagation of a single nucleus given by the MV model. In the continuum such a field configuration
    should stay static while moving at the speed of light. On the lattice, due to numerical dispersion, the field
    becomes unstable and leads to an unphysical exponential energy increase.

    We look at the performance of different numerical schemes for lattice Yang-Mills, in particular the standard
    leapfrog solver and our new semi-implicit with regards to this numerical instability.
"""
max_steps = 43
iterations = [10, 20, 30, 40, 50, 15, 25, 35, 45]
damping = 0.45
step = 1
fn = 'si_gauss.dat'
n_l = 128
n_t = 256

"""
    Initial conditions parameters
"""

# Lattice size

nx, ny, nz = n_l, n_t, n_t

# Transverse and longitudinal box widths [fm]
LT = 6.0
LL = 1.5

# Collision energy [MeV]
sqrts = 200.0 * 1000.0

# Infrared and ultraviolet regulator [MeV]
m = 200.0
uv = 20.0 * 1000.0

"""
    The rest of the parameters are computed automatically.
"""

# Constants
hbarc = 197.3270  # hbarc [MeV*fm]
RAu = 7.27331  # Gold nuclear radius [fm]

# Determine lattice spacings and energy units
aT_fm = LT / ny
E0 = hbarc / aT_fm
aT = 1.0
aL_fm = LL / nx
aL = aL_fm / aT_fm
a = [aL, aT, aT]
dt = aL/step

# Determine initial condition parameters in lattice units
gamma = sqrts / 2000.0
Qs = np.sqrt((sqrts / 1000.0) ** 0.25) * 1000.0
alphas = 12.5664 / (18.0 * np.log(Qs / 217.0))
g = np.sqrt(12.5664 * alphas)
mu = Qs / (g * g * 0.75) / E0
uvt = uv / E0
ir = m / E0
sigma = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = max_steps
nx += 2

numpy.random.seed(1)
DEBUG = True

dims = [nx, ny, nz]
iter_dims = [1, nx-1, 0, ny, 0, nz]

data = []

for current_iterations in iterations:
    s = SimulationImplicit(dims, iter_dims, a, dt, g)
    s.mode = 1
    s.damping = damping
    s.iterations = current_iterations

    s.debug = DEBUG

    s.log("Starting simulation at", datetime.now())
    s.log("Memory use:", int(s.get_nbytes() / (1024 * 1024)), "mb")

    t = time.time()
    s.log("Initializing left nucleus.")
    v = mv.initialize_mv(s=s, x0=int(nx * 1.0/3.0 * aL), mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
    interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
    s.log("Initialized left nucleus.", round(time.time() - t, 3), "s")
    s.init()
    s.log("Memory use:", int(s.get_nbytes() / (1024 * 1024)), "mb")

    gauss_initial = pyglasma3d.core.semiimplicit.gauss_constraint_violation(s)
    print("Initial value", gauss_initial)

    # loop
    for it in range(max_iters):
        # this for loop moves the nuclei exactly one grid cell
        for st in range(step):
            t = time.time()
            s.evolve()
            s.log("Complete cycle.", round(time.time() - t, 3), "s")
            t = time.time()
    s.log("Completed simulation at", datetime.now())

    gauss_final = pyglasma3d.core.semiimplicit.gauss_constraint_violation(s)
    print("Final value", gauss_final)

    data.append(np.array([current_iterations, damping, gauss_initial, gauss_final, max_iters]))
    np.savetxt('./output/' + fn, np.array(data), fmt="%d %1.4f %1.12e %1.12e %d")

exit()
