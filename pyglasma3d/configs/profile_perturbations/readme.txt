Scripts in this folder deal with the effect of perturbed longitudinal profiles on the rapidity profiles of the Glasma.

We look at collisions of nuclei with finite longitudinal width, where the longitudinal profile of one of the nuclei
is slightly deformed. We expect these perturbations of the initial conditions to affect the rapidity profiles in certain
rapidity directions after the classical Yang-Mills evolution of the collision is finished.