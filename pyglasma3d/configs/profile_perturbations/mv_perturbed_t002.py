from __future__ import print_function

import numpy as np
import time

import pyglasma3d.cy.mv as mv
import pyglasma3d.cy.interpolate as interpolate

from pyglasma3d.core import Simulation

"""
    Collisions of two nuclei with perturbations of the longitudinal profile

    One of the nuclei is perturbed: the original Gaussian shape is slightly deformed and superimposed with a second,
    smaller Gaussian profile, which is slightly shifted.

    The simulation parameters are similar to arXiv:1703.00017.

    The script simulates a certain number of events and performs data averaging after all events are completed.
"""

run = 'mv_perturb_t002'    # data will end up in `./output/T_<run>_e<num>.dat`
num_events = 5

# grid size (make sure that ny == nz)
nx, ny, nz = 1024, 128, 128

# transverse and longitudinal box widths [fm]
LT = 4.0
LL = 4.0

# collision energy [MeV]
sqrts = 200.0 * 1000.0

# infrared and ultraviolet regulator [MeV]
m = 400.0
uv = 10.0 * 1000.0

# ratio between dt and aL [int, multiple of 2]
steps = 2

# option for debug
debug = True

# perturbation options
use_perturbation = True    # set this to perturb the right nucleus
amplitude_perturbed = 0.2  # amplitude of the perturbation compared to the original fields
shift_perturbed_ratio = 3  # shift in longitudinal coordinate in units of sigma
sigma_perturbed_ratio = 1  # width of the perturbation in units of sigma


"""
    The rest of the parameters are computed automatically.
"""

# constants
hbarc   = 197.3270      # hbarc [MeV*fm]
RAu     = 7.27331       # Gold nuclear radius [fm]

# determine lattice spacings and energy units
aT_fm   = LT / ny
E0      = hbarc / aT_fm
aT      = 1.0
aL_fm   = LL / nx
aL      = aL_fm / aT_fm
a       = [aL, aT, aT]
dt      = aL / steps

# determine initial condition parameters
gamma   = sqrts / 2000.0
Qs      = np.sqrt((sqrts/1000.0) ** 0.25) * 1000.0
alphas  = 12.5664 / (18.0 * np.log(Qs / 217.0))
g       = np.sqrt(12.5664 * alphas)
mu      = Qs / (g * g * 0.75) / E0
uvt     = uv / E0
ir      = m / E0
sigma   = RAu / (2.0 * gamma) / aL_fm * aL
sigma_c = sigma / aL

# number of evolution steps (max_iters) and output file path
max_iters = 2 * nx / 3
file_path = './output/T_' + run + '.dat'


"""
    Loop over events
"""
for ev in range(num_events):

    """
        Initialization
    """

    # initialize simulation object
    dims = [nx, ny, nz]
    s = Simulation(dims=dims, a=a, dt=dt, g=g, iter_dims=[1, nx-1, 0, ny, 0, nz])
    s.debug = True

    s.log("STARTING EVENT NO. " + str(ev))

    t = time.time()
    s.log("Initializing left nucleus.")
    v = mv.initialize_mv(s, x0=nx * 0.25 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=+1)
    interpolate.initialize_charge_planes(s, +1, 0, nx / 2)
    s.log("Initialized left nucleus.", round(time.time()-t, 3))

    if use_perturbation:
        t = time.time()
        s.log("Initializing right (perturbed) nucleus.")
        v = mv.initialize_mv_perturbed(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1,
                                       amplitude_perturbed=0.2,
                                       shift_perturbed=shift_perturbed_ratio*sigma,
                                       sigma_perturbed=sigma_perturbed_ratio*sigma)
        interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
        s.log("Initialized right (perturbed) nucleus.", round(time.time()-t, 3))
    else:
        t = time.time()
        s.log("Initializing right (unperturbed) nucleus.")
        v = mv.initialize_mv(s, x0=nx * 0.75 * aL, mu=mu, sigma=sigma, mass=ir, uvt=uvt, orientation=-1)
        interpolate.initialize_charge_planes(s, -1, nx / 2, nx)
        s.log("Initialized right (unperturbed) nucleus.", round(time.time()-t, 3))

    s.init()

    """
        Simulation loop
    """

    file_path = './output/T_' + run + '_e' + str(ev) + '.dat'

    t = time.time()
    for it in range(max_iters):
        # this for loop moves the nuclei exactly one grid cell
        for step in range(steps):
            t = time.time()
            s.evolve()
            print(s.t, "Complete cycle.", round(time.time()-t, 3))

        t = time.time()

        s.write_energy_momentum(max_iters, file_path)
        print(s.t, "Writing energy momentum tensor to file.", round(time.time()-t, 3))

"""
    Data averaging
"""

# import all output files and average
file_path = './output/T_' + run + '_e' + str(0) + '.dat'
f = open(file_path, "r")

# read header
header = np.fromfile(f, count=2, dtype=np.int64)

# read rest of data
f.seek(16)

data_avg = np.fromfile(f, dtype=np.double)
f.close()

for ev in range(1, num_events):
    file_path = './output/T_' + run + '_e' + str(ev) + '.dat'
    f = open(file_path, "r")

    # read rest of data
    f.seek(16)

    data = np.fromfile(f, dtype=np.double)
    data_avg += data
    f.close()

data_avg /= num_events

# write to new file with averaged data
file_path = './output/T_' + run + '_avg' + '.dat'
f = open(file_path, "w+")
f.write(header)
f.write(data_avg)
f.close()
