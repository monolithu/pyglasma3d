#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A module implementing the (proper) implicit solver for lattice Yang-Mills, which can be derived from an action
    principle and conserves a variant of the Gauss constraint.

    We use a damped iterative approach to solving the implicit equations.
"""
cimport lattice as la

cimport solver_implicit_common as ccommon
import solver_implicit_common as common

from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
import numpy as np
cimport numpy as cnp
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt
from cpython cimport bool
from time import time

def evolve_iterate(s, iterations, damping):
    # iteration for stabilization
    combined_evolution(s, iterations, damping)

def evolve_initial(s):
    # initial guess from leapfrog
    common.evolve_initial(s)

def combined_evolution(s, int iterations, double damping):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double alpha = damping

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index, k

    # temporary buffers
    cdef double *group_buffer
    cdef double *algebra_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]
    cdef int current_iteration

    for current_iteration in range(iterations):
        # evolve electric field
        with nogil, parallel():
            group_buffer = <double *> malloc(4 * sizeof(double))
            algebra_buffer = <double *> malloc(3 * sizeof(double))

            for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
                for di in range(3):
                    la.algebra_zero(algebra_buffer)

                    field_index = la.get_field_index(cell_index, di)
                    group_index = la.get_group_index(cell_index, di)

                    if di == 0:
                        longitudinal_eom_terms(cell_index, &dims[0], &acc[0], &spacings[0], dt, &u0[0], &u1[0], &u2[0], &algebra_buffer[0])
                    else:
                        transverse_eom_terms(cell_index, di, &dims[0], &acc[0], &spacings[0], dt, &u0[0], &u1[0], &u2[0], &algebra_buffer[0])

                    # damped iterative solver
                    la.algebra_mul(&e1[field_index], alpha)
                    la.algebra_add(&e1[field_index], &e0[field_index], 1.0 - alpha)
                    la.algebra_add(&e1[field_index], algebra_buffer, 1.0 - alpha)

                la.algebra_add(&e1[la.get_field_index(cell_index, 0)], &j[3 * cell_index], (1.0 - alpha) * current_factor)

            free(group_buffer)
            free(algebra_buffer)
        # evolve gauge links
        with nogil, parallel():
            group_buffer = <double *> malloc(4 * sizeof(double))
            for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
                for di in range(3):
                    field_index = la.get_field_index(cell_index, di)
                    group_index = la.get_group_index(cell_index, di)

                    for k in range(3):
                        group_buffer[k+1] = - 0.5 * dt * e1[field_index+k]

                    # unitarize
                    group_buffer[0] = 0.25 * la.algebra_sq(&e1[field_index]) * dt ** 2
                    group_buffer[0] = sqrt(1.0 - group_buffer[0])

                    la.mul2_fast(group_buffer, &u1[group_index], &u2[group_index])
            free(group_buffer)

# Gauss constraint, averaged across the lattice
def gauss_constraint(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] g_density
    g_density = gauss_density(s)
    cdef double result = 0.0
    cdef long n = s.N
    cdef long x

    with nogil:
        for x in prange(n):
            result += la.algebra_sq(&g_density[3*x])

    return result / n

# Gauss constraint violation for paper
def gauss_constraint_violation(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[double, ndim=1, mode="c"] g_density
    g_density = gauss_density(s)
    cdef double constraint = 0.0
    cdef double density = 0.0
    cdef long n = s.N
    cdef long x

    with nogil:
        for x in prange(n):
            constraint += la.algebra_sq(&g_density[3*x])
            density += la.algebra_sq(&rho[3*x])

    return constraint / density

# Gauss density
def gauss_density(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"]   u0 = s.up
        cnp.ndarray[double, ndim=1, mode="c"]   u1 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"]   u2 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"]   e0 = s.ep
        cnp.ndarray[double, ndim=1, mode="c"]   e1 = s.e
        cnp.ndarray[double, ndim=1, mode="c"]   rho = s.r
        cnp.ndarray[long, ndim=1, mode="c"]     dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"]     acc = s.acc
        cnp.ndarray[long, ndim=1, mode="c"]     iter_dims = s.iter_dims
        cnp.ndarray[double, ndim=1, mode="c"]   a = s.a

        long nx0, nx1, ny0, ny1, nz0, nz1
        double g = s.g
        double a0 = s.dt

        long x, i
        double * buffer0
        double * buffer1
        double * buffer2

        cnp.ndarray[double, ndim=1, mode="c"] g_density = np.zeros(3*s.N, dtype=np.double)

    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    with nogil, parallel():
        buffer0 = <double *> malloc(4 * sizeof(double))
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))


        for x in prange((nx0+1) * dims[1] * dims[2], (nx1-1) * dims[1] * dims[2]):
            la.algebra_zero(buffer2)
            # Standard electric contribution D_i E_x,i - rho
            # ... Using temporal plaquettes
            for i in range(3):
                ccommon.tplaq(&u1[0], &u2[0], x, i, +1, &dims[0], &acc[0], buffer0)
                ccommon.tplaq(&u1[0], &u2[0], x, i, -1, &dims[0], &acc[0], buffer1)
                la.group_add(buffer0, buffer1, 1)
                la.proj(buffer0, buffer1)
                la.algebra_add(buffer2, buffer1, -a0 / (a0 * a[i]) ** 2 / g)

            # Charge density
            la.algebra_add(buffer2, &rho[3*x], -1.0)

            # Contribution from implicit transverse plaquettes
            gauss_implicit_terms(x, &u0[0], &u1[0], &u2[0], &dims[0], &acc[0], &a[0], a0, g, buffer2)

            # Contributions from semi-implicit longitudinal-transverse plaquettes
            gauss_semi_implicit_terms(x, &u0[0], &u1[0], &u2[0], &dims[0], &acc[0], &a[0], a0, g, buffer2)

            # Write to result vector
            la.algebra_add(&g_density[3*x], buffer2, 1)

        free(buffer0)
        free(buffer1)
        free(buffer2)

    return g_density

"""
    C functions
"""
cdef inline void longitudinal_eom_terms(long x, long *dims, long *acc, double *a, double dt, double *u0, double *u1, double *u2, double*r) nogil:
    cdef long i, oi
    cdef double[4] b1, b2, b3

    la.group_zero(b3)
    for oi in range(-1, 3, 2):
        for i in range(1, 3):
            # \avg{T[U, C]}_{x,1i}
            kay(x, 0, i, 1, oi, u0, u0, u0, dims, acc, a, 0.25, 0, 0, b1)
            la.group_add(b3, b1, 1)
            kay(x, 0, i, 1, oi, u2, u2, u2, dims, acc, a, 0.25, 0, 0, b1)
            la.group_add(b3, b1, 1)

            # T[U,W]_{x, 1i}
            kay(x, 0, i, 1, oi, u0, u1, u2, dims, acc, a, 0.5, 2, 0, b1)
            la.group_add(b3, b1, 1)

    la.mul2(&u1[la.get_group_index(x, 0)], b3, 1, 1, b2)
    la.proj(b2, r)
    la.algebra_mul(r , -0.5 * dt)

cdef inline void transverse_eom_terms(long x, long i, long *dims, long *acc, double *a, double dt, double *u0, double *u1, double *u2, double*r) nogil:
    cdef long j, oj, o
    cdef double[4] b1, b2, b3

    la.group_zero(b3)

    # implicit contribution
    j = (i % 2) + 1

    for oj in range(-1, 3, 2):
        kay(x, i, j, 1, oj, u0, u1, u2, dims, acc, a, 1.0, 1, 0, b1)
        la.group_add(b3, b1, 1)

    # semi-implicit contribution
    for o in range(-1, 3, 2):
        kay(x, i, 0, 1, o, u0, u1, u2, dims, acc, a, 0.5, 0, 1, b1)
        la.group_add(b3, b1, 1)

        kay(x, i, 0, 1, o, u0, u1, u2, dims, acc, a, 0.5, 2, 0, b1)
        la.group_add(b3, b1, 1)

    la.mul2(&u1[la.get_group_index(x, i)], b3, 1, 1, b2)
    la.proj(b2, r)
    la.algebra_mul(r , - 0.5 * dt)

"""
    Computes the C^t C term in the Gauss constraint
"""
cdef inline void gauss_implicit_terms(long x, double*u0, double*u1, double*u2, long*dims, long*acc, double*a, double dt, double g, double*r) nogil:
    cdef:
        double[4] buffer0, buffer1, buffer2
    #la.algebra_zero(r)
    for i in range(1, 3):
        for j in range(1, 3):
            if i != j:
                for oi in range(-1, 3, 2):
                    for oj in range(-1, 3, 2):
                        ccommon.c(&u1[0], x, i, j, oi, oj, &dims[0], &acc[0], buffer0)
                        ccommon.c(&u2[0], x, i, j, oi, oj, &dims[0], &acc[0], buffer1)
                        la.mul2(buffer1, buffer0, +1, -1, buffer2)
                        la.proj(buffer2, buffer0)
                        la.algebra_add(r, buffer0, -dt / (8.0 * (a[i] * a[j]) ** 2) / g)

"""
    Computes the U^t V terms in the Gauss constraint
"""
cdef inline void gauss_semi_implicit_terms(long x, double*u0, double*u1, double*u2, long*dims, long*acc, double*a, double dt, double g, double*r) nogil:
    cdef double[4] buffer0, buffer1
    cdef long xs

    #la.algebra_zero(r)

    xs = la.shift2(x, 0, -1, dims, acc)
    la.group_zero(buffer0)

    u_v_dagger(x,  u2, u1, +1, dims, acc, a, +1, buffer0)
    u_v_dagger(x,  u1, u2, +1, dims, acc, a, -1, buffer0)
    u_v_dagger(xs, u2, u1, -1, dims, acc, a, +1, buffer0)
    u_v_dagger(xs, u1, u2, -1, dims, acc, a, -1, buffer0)
    la.proj(buffer0, buffer1)
    la.algebra_add(r, buffer1, -0.125 * dt / a[0] ** 2 / g)

cdef inline void u_v_dagger(long x, double*u_u, double*u_v, int dagger, long *dims, long*acc, double *a, double factor, double *r) nogil:
    cdef double[4] buffer0, buffer1
    v_dagger(x, u_v, dims, acc, a, buffer0)
    la.mul2(&u_u[la.get_group_index(x, 0)], buffer0, dagger, dagger, buffer1)
    la.group_add(r, buffer1, factor)

cdef inline void v_dagger(long x, double *u, long *dims, long *acc, double *a, double *r) nogil:
    cdef long j, xs, xs2, oj
    cdef double[4] buffer0, buffer1, buffer2
    xs = la.shift2(x, 0, 1, dims, acc)
    la.group_zero(r)
    for oj in range(-1, 3, 2):
        for j in range(1, 3):
            ccommon.c(u, x, 0, j, 1, 1, dims, acc, buffer1)
            la.mul2(&u[la.get_group_index(xs, j)], buffer1, 1, -1, buffer0)
            xs2 = la.shift2(x, j, -1, dims, acc)
            ccommon.c(u, xs2, 0, j, 1, 1, dims, acc, buffer1)
            la.mul2(buffer1, &u[la.get_group_index(xs2, j)], -1, 1, buffer2)
            la.group_add(buffer0, buffer2, -1.0)
            la.group_add(r, buffer0, 1.0 / a[j] ** 2)

# convenience function for -0.5 /(a^i a^j)^2 (U_{x+i,j} C^t_{x,ij} - C^t_{x-j,ij} U_{x-j,j})
# mode1 selects between C (0), M (1) or W (2)
# mode2 selects between U (0) or U_bar (1)
cdef inline void kay(long x, long i, long j, long oi, long oj, double*u0, double*u1, double*u2,
            long*dims, long*acc, double*a, double factor, int mode1, int mode2, double*r) nogil:
    cdef long xp, xm, xr, gr
    cdef double[4] F1, F2, L1, L2, b1, b2
    cdef double mul_factor

    xp = la.shift2(x, i, oi, dims, acc)
    xm = la.shift2(x, j, -oj, dims, acc)

    # compute F
    if mode1 == 0:
        ccommon.c(u1, x,  i, j, oi, oj, dims, acc, F1)
        ccommon.c(u1, xm, i, j, oi, oj, dims, acc, F2)
    elif mode1 == 1:
        ccommon.m(u0, u2, x,  i, j, oi, oj, dims, acc, F1)
        ccommon.m(u0, u2, xm, i, j, oi, oj, dims, acc, F2)
    elif mode1 == 2:
        w2(x,  i, j, oi, oj, u0, u1, u2, dims, acc, F1)
        w2(xm, i, j, oi, oj, u0, u1, u2, dims, acc, F2)

    # compute L
    if mode2 == 0:
        ccommon.link(xp, j, oj, dims, acc, &xr, &gr)
        la.group_set(L1, &u1[gr])
        ccommon.link(xm, j, oj, dims, acc, &xr, &gr)
        la.group_set(L2, &u1[gr])
    elif mode2 == 1:
        ccommon.u_bar(u0, u2, xp, j, oj, dims, acc, L1)
        ccommon.u_bar(u0, u2, xm, j, oj, dims, acc, L2)

    # L1 * F1
    la.mul2(L1, F1, oj, -1, b1)

    # F2 * L2
    la.mul2(F2, L2, -1, oj, b2)

    mul_factor = factor / (a[j] ** 2)
    la.group_zero(r)
    la.group_add(r, b1, +mul_factor)
    la.group_add(r, b2, -mul_factor)

# more general function for W
cdef void w2(long x, long i, long j, long oi, long oj,
             double*u0, double*u1, double*u2, long*dims, long*acc, double*r) nogil:
    cdef double[4] l1, l2, b1, b2
    cdef long xs, xr, gr
    la.group_zero(r)
    # W_{x,1j}
    if i == 0:
        ccommon.u_bar(u0, u2, x, i, oi, dims, acc, l1)
        xs = la.shift2(x, i, oi, dims, acc)
        ccommon.link(xs, j, oj, dims, acc, &xr, &gr)
        la.mul2(l1, &u1[gr], oi, oj, r)

        ccommon.link(x, j, oj, dims, acc, &xr, &gr)
        ccommon.u_bar(u0, u2, xr, i, oi, dims, acc, l1)
        la.mul2(&u1[gr], l1, oj, oi, b1)
        la.group_add(r, b1, -1)
    # W_{x,i1}
    elif j==0:
        ccommon.link(x, i, oi, dims, acc, &xr, &gr)
        ccommon.u_bar(u0, u2, xr, j, oj, dims, acc, l1)
        la.mul2(&u1[gr], l1, oi, oj, r)

        ccommon.u_bar(u0, u2, x, j, oj, dims, acc, l1)
        xs = la.shift2(x, j, oj, dims, acc)
        ccommon.link(xs, i, oi, dims, acc, &xr, &gr)
        la.mul2(l1, &u1[gr], oj, oi, b1)
        la.group_add(r, b1, -1)
