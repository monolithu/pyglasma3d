#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
"""
    A module related to the early version of the semi-implicit YM solver.
    The equations of motion were just a guess based on an analogue scheme electromagnetic
    and do not conserve the Gauss constraint. It's also likely that this solver can not
    be derived from a discrete action principle.
"""

from lattice cimport *
cimport solver_implicit_common as ccommon
import solver_implicit_common as common

cimport cython
cimport openmp
import cython
import numpy as np
cimport numpy as cnp
cimport openmp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

def evolve_iterate(s, iterations, damping):
    # iteration for stabilization
    cdef int n
    for n in range(iterations):
        evolve_electric(s, damping)
        common.evolve_links2(s)

def evolve_initial(s):
    # initial guess from leapfrog
    common.evolve_initial(s)

def evolve_electric(s, damping):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt
    cdef double alpha = 1.0 - damping

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index

    # temporary buffers
    cdef double *group_buffer
    cdef double *algebra_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double plaq_factor[3]
    for dj in range(3):
        plaq_factor[dj] = dt / (spacings[dj] * spacings[dj])

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]

    with nogil, parallel():
        group_buffer = <double *> malloc(4 * sizeof(double))
        algebra_buffer = <double *> malloc(3 * sizeof(double))

        for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
            for di in range(3):
                # sum over plaquettes (positive and negative) and include contributions from past and future
                algebra_zero(algebra_buffer)

                # field index for electric field
                field_index = get_field_index(cell_index, di)

                compute_plaquettes(cell_index, di, &u0[0], &u1[0], &u2[0], &dims[0], &acc[0], &plaq_factor[0], algebra_buffer)
                algebra_mul(&e1[field_index], alpha)
                algebra_add(&e1[field_index], &e0[field_index], 1.0 - alpha)
                algebra_add(&e1[field_index], algebra_buffer, 1.0 - alpha)

                if di == 0:
                    algebra_add(&e1[field_index], &j[3 * cell_index], (1.0 - alpha) * current_factor)

        free(group_buffer)
        free(algebra_buffer)

"""
    C functions
"""

cdef void compute_plaquettes(long x, long d, double*u0, double*u1, double*u2,
                             long*dims, long*acc,
                             double*plaq_factor, double *result) nogil:
    cdef double buffer0[4]
    cdef double buffer1[4]
    cdef double buffer_S[4]
    cdef long i
    group_zero(&buffer_S[0])

    if d == 0:
        # d == 0 (longitudinal direction)
        for i in range(1, 3):
            # mixed plaquettes
            # past contribution (with transverse links from present)
            group_zero(&buffer1[0])
            simple_plaquette_sum(x, d, i, dims, acc, u1, u0, &buffer0[0])
            group_add(&buffer1[0], &buffer0[0], 0.5)

            # future contribution (with transverse links from present)
            simple_plaquette_sum(x, d, i, dims, acc, u1, u2, &buffer0[0])
            group_add(&buffer1[0], &buffer0[0], 0.5)
            group_add(&buffer_S[0], &buffer1[0], plaq_factor[i])
    else:
        # d != 0 (transverse direction)
        for i in range(3):
            if i != d:
                if i == 0:
                    # mixed plaquettes
                    # past contribution (with transverse links from present)
                    group_zero(&buffer1[0])
                    simple_plaquette_sum(x, d, i, dims, acc, u0, u1, &buffer0[0])
                    group_add(&buffer1[0], &buffer0[0], 0.5)

                    # future contribution (with transverse links from present)
                    simple_plaquette_sum(x, d, i, dims, acc, u2, u1, &buffer0[0])
                    group_add(&buffer1[0], &buffer0[0], 0.5)
                    group_add(&buffer_S[0], &buffer1[0], plaq_factor[i])
                else:
                    # transverse plaquettes
                    # past contribution
                    group_zero(&buffer1[0])
                    simple_plaquette_sum(x, d, i, dims, acc, u0, u0, &buffer0[0])
                    group_add(&buffer1[0], &buffer0[0], 0.5)

                    # future contribution
                    simple_plaquette_sum(x, d, i, dims, acc, u2, u2, &buffer0[0])
                    group_add(&buffer1[0], &buffer0[0], 0.5)
                    group_add(&buffer_S[0], &buffer1[0], plaq_factor[i])

    proj(&buffer_S[0], &result[0])


cdef void transverse_plaquette(long x, long i, long j, long*dims, long*acc, double *u0, double *u1, double *u2, double *result) nogil:
    cdef double buffer0[4]
    group_zero(result)

    # past contribution
    simple_plaquette_sum(x, i, j, dims, acc, u0, u0, &buffer0[0])
    group_add(result, &buffer0[0], 0.5)

    # future contribution
    simple_plaquette_sum(x, i, j, dims, acc, u2, u2, &buffer0[0])
    group_add(result, &buffer0[0], 0.5)

cdef void mixed_plaquette(long x, long i, long j, long*dims, long*acc, double *u0, double *u1, double *u2, double *result) nogil:
    cdef double buffer0[4]
    group_zero(result)

    # past contribution (with present transverse links)
    simple_plaquette_sum(x, i, j, dims, acc, u1, u0, &buffer0[0])
    group_add(result, &buffer0[0], 0.5)

    # future contribution (with present transverse links)
    simple_plaquette_sum(x, i, j, dims, acc, u1, u2, &buffer0[0])
    group_add(result, &buffer0[0], 0.5)


cdef void simple_plaquette_sum(long x, long i, long j, long*dims, long*acc, double *U, double *V, double *result) nogil:
    cdef long ci0, ci1, ci2, ci3, ci4
    cdef double buffer0[4]
    cdef double buffer1[4]
    cdef double buffer2[4]

    ci0 = x
    ci1 = shift2(ci0, i, 1, dims, acc)
    ci2 = shift2(ci0, j, 1, dims, acc)
    ci3 = shift2(ci1, j, -1, dims, acc)
    ci4 = shift2(ci0, j, -1, dims, acc)

    group_zero(buffer0)
    group_zero(buffer1)
    group_zero(buffer2)
    group_zero(result)

    # upper staple
    mul2(&U[4 * (3 * ci1 + j)], &V[4 * (3 * ci2 + i)], 1, -1, &buffer0[0])
    mul2(&buffer0[0], &U[4 * (3 * ci0 + j)], 1, -1, &buffer1[0])
    group_set(&buffer2[0], &buffer1[0])

    # lower staple
    mul2(&U[4 * (3 * ci3 + j)], &V[4 * (3 * ci4 + i)], -1, -1, &buffer0[0])
    mul2(&buffer0[0], &U[4 * (3 * ci4 + j)], 1, 1, &buffer1[0])
    group_add(&buffer2[0], &buffer1[0], 1.0)

    # multiply with missing link to complete plaquettes
    mul2(&V[4 * (3 * ci0 + i)], &buffer2[0], 1, 1, &buffer0[0])
    group_set(result, &buffer0[0])

