#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    CPIC Interpolation routines
        * creation of charge planes
        * charge refinement
        * charge movement and current interpolation
"""

cimport cython
import cython
from lattice cimport *
from gauss cimport *
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt, erf
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

# sample gauss density and create charge planes
def initialize_charge_planes(s, long orientation, long nx0, long nx1):
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = np.zeros(6, dtype=np.int64)
    # restricted region
    iter_dims = s.iter_dims.copy()
    iter_dims[0] = nx0
    iter_dims[1] = nx1

    # redefine stuff from simulation object. compiler will complain otherwise.
    cdef cnp.ndarray[double, ndim=1, mode="c"] gauss_d = np.zeros(3 * s.N, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] links = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc

    # compute gauss density, put it into gauss_d
    # if s.mode is defined then we are dealing with an implicit scheme
    if s.mode is None:
        gauss_density(&links[0], &electric[0], &rho[0],
                      &spacings[0], &dims[0], &acc[0], iter_dims, s.g,
                      &gauss_d[0])
    else:
        gauss_d = s.gauss_density()


    # average gauss density across transverse plane and determine cutoff charge
    cdef cnp.ndarray[double, ndim=1, mode="c"] gauss_d_avg = np.zeros(3 * dims[0], dtype=np.double)
    cdef long ix, iy, iz, x
    for ix in prange(nx0, nx1, nogil=True):
        for iy in range(iter_dims[2], iter_dims[3]):
            for iz in range(iter_dims[4], iter_dims[5]):
                x = get_index(ix, iy, iz, &dims[0])
                gauss_d_avg[ix] += sqrt(algebra_sq(&gauss_d[3 * x]))
    gauss_d_avg /= dims[1] * dims[2]

    cdef double gauss_avg_max = np.max(gauss_d_avg)
    cdef double gauss_cutoff = gauss_avg_max * 1.0E-10

    # restrict region according to gauss_cutoff
    for ix in range(nx0, nx1):
        if gauss_d_avg[ix] >= gauss_cutoff:
            nx0 = ix
            break

    for ix in range(nx1, nx0, -1):
        if gauss_d_avg[ix] >= gauss_cutoff:
            nx1 = ix
            break

    # perform charge refinement
    cdef long particles_per_cell = np.round(spacings[0] / s.dt)
    cdef long number_of_planes = (nx1 - nx0) * particles_per_cell
    cdef long particles_per_plane = dims[1] * dims[2]
    cdef long i, xt, j, n

    # reorder gauss density array for easier refinement
    gauss_d = np.swapaxes(np.reshape(gauss_d, (dims[0], dims[1], dims[2], 3)), 0, -1).flatten()
    gauss_d = np.repeat(gauss_d, particles_per_cell) / particles_per_cell
    # order is now (3, dims[1], dims[2], dims[0] * ppc)
    # this means that the charge lines of length 'dims[0] * ppc' are laid out correctly in memory

    # apply refinement
    cdef cnp.ndarray[double, ndim=1, mode="c"] gauss_line
    cdef long ppc = particles_per_cell
    n = 3 * s.N * ppc
    cdef long ni = 3 * particles_per_plane
    cdef long n_line = dims[0] * ppc
    cdef long nx, ny, nz
    nx, ny, nz = dims
    for iy in prange(ny, nogil=True, schedule="guided"):
        for iz in range(nz):
            xt = iy * dims[2] + iz
            for j in range(3):
                i = j * dims[1] * dims[2] * n_line + xt * n_line
                refine2(&gauss_d[i], ppc * nx0, ppc * nx1, 100, ppc)
                refine4(&gauss_d[i], ppc * nx0, ppc * nx1, 100, ppc)

    # put array back into order
    gauss_d = np.swapaxes(np.reshape(gauss_d, (3, dims[1], dims[2], dims[0], ppc)), 3, 0).flatten()
    gauss_d = np.moveaxis(np.reshape(gauss_d, (dims[0], dims[1], dims[2], 3, ppc)), -1, 0).flatten()

    # place charge planes
    cdef cnp.ndarray[double, ndim=1, mode="c"] plane_charges
    cdef cnp.ndarray[double, ndim=1, mode="c"] charges = np.zeros(3 * dims[1] * dims[2], dtype=np.double)
    n = 3 * (<long> s.N)
    from pyglasma3d.core import ChargePlane
    for i in range(ppc):
        for ix in range(nx0, nx1):
            pl = ChargePlane(dims, ix * particles_per_plane, i, orientation)
            plane_charges = pl.q
            for iy in range(0, dims[1]):
                for iz in range(0, dims[2]):
                    x = get_index(ix, iy, iz, &dims[0])
                    xt = iy * dims[2] + iz
                    plane_charges[3 * xt + 0] = gauss_d[i * n + 3 * x + 0]
                    plane_charges[3 * xt + 1] = gauss_d[i * n + 3 * x + 1]
                    plane_charges[3 * xt + 2] = gauss_d[i * n + 3 * x + 2]
            s.charges.append(pl)

# second order charge refinement
cdef void refine2(double*gl, long n0, long n1, long n_iter, long ppc) nogil:
    cdef long it, i
    cdef double dq
    for it in range(n_iter):
        for i in range(n0 + 1, n1 - 2):
            if i % ppc < ppc - 1:
                dq = 0.25 * (-gl[i - 1] + 3 * gl[i] - 3 * gl[i + 1] + gl[i + 2])
                gl[i] -= dq
                gl[i + 1] += dq

# fourth order charge refinement
cdef void refine4(double*gl, long n0, long n1, long n_iter, long ppc) nogil:
    cdef long it, i
    cdef double dq
    for it in range(n_iter):
        for i in range(n0 + 2, n1 - 3):
            if i % ppc < ppc - 1:
                dq = (gl[i - 2] - 5 * gl[i - 1] + 10 * gl[i] - 10 * gl[i + 1] + 5 * gl[i + 2] - gl[i + 3]) / 12.0
                gl[i] -= dq
                gl[i + 1] += dq

# interpolate charge planes to lattice charge density
def interpolate_to_charge_density(s):
    cdef long x, index_offset, num
    cdef cnp.ndarray[double, ndim=1, mode="c"] charges
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    # reset charge density
    cdef long n = <long> s.N
    for x in prange(0, 3 * n, nogil=True):
        rho[x] = 0.0

    # interpolate all charge planes to rho array
    for pl in s.charges:
        charges = pl.q
        index_offset = pl.index_offset
        num = pl.N
        interpolate_charges(&charges[0], index_offset, num, &rho[0])

# auxiliary function for above
cdef inline void interpolate_charges(double *charges, long x, long n, double *rho):
    cdef long i
    cdef double *shifted_rho = &rho[3 * x]
    for i in prange(0, 3 * n, nogil=True, schedule="guided"):
        shifted_rho[i] += charges[i]

# move charge planes and interpolate currents on the lattice
def advance_and_interpolate_currents(s):
    cdef long x
    cdef double f = s.a[0] / s.dt
    cdef cnp.ndarray[double, ndim=1, mode="c"] charges = s.r
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef long particles_per_cell = np.round(s.a[0] / s.dt)

    # reset charge density
    cdef long n = s.N
    for x in prange(0, 3 * n, nogil=True):
        current[x] = 0.0

    # not very elegant, but this makes sure that the interpolation works for both the old leapfrog code
    # and the implicit schemes
    if s.mode is not None:
        links0, links1 = links1, links0

    # check if charge plane has moved and interpolate if needed
    cdef long ppcmo = particles_per_cell - 1
    cdef long pln = s.dims[1] * s.dims[2]
    cdef long nmax = (s.dims[0] - 1) * s.dims[1] * s.dims[2]
    cdef long nmin = s.dims[1] * s.dims[2]
    for pl in s.charges:
        if pl.active is True:
            charges = pl.q
            pl.subcell_shift = mod(pl.subcell_shift + pl.orientation, particles_per_cell)
            # positive orientation
            if pl.orientation > 0:
                if pl.subcell_shift == 0:
                    # interpolate to grid and do parallel transport
                    interpolate_currents_pos(&links1[0], &charges[0], pl.index_offset, pl.N, f, &current[0])
                    pl.index_offset += pln
                    if pl.index_offset >= nmax:
                        pl.active = False
                        continue
            else:
                if pl.subcell_shift == ppcmo:
                    # interpolate to grid and do parallel transport
                    pl.index_offset -= pln
                    interpolate_currents_neg(&links1[0], &charges[0], pl.index_offset, pl.N, -f, &current[0])
                    if pl.index_offset <= nmin:
                        pl.active = False
                        continue

# auxiliary functions for above
cdef inline void interpolate_currents_pos(double *links, double *charges, long x, long n, double f, double *current):
    cdef long i
    for i in prange(0, n, nogil=True, schedule="guided"):
        algebra_add(&current[3 * (x + i)], &charges[3 * i], f)
        act(&links[4 * 3 * (x + i)], &charges[3 * i], 1, &charges[3 * i])

cdef inline void interpolate_currents_neg(double *links, double *charges, long x, long n, double f, double *current):
    cdef long i
    for i in prange(0, n, nogil=True, schedule="guided"):
        act(&links[4 * 3 * (x + i)], &charges[3 * i], -1, &charges[3 * i])
        algebra_add(&current[3 * (x + i)], &charges[3 * i], f)
