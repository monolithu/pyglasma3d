#cython: boundscheck=False, wraparound=False, nonecheck=False

"""
    Gauss constraint functions
"""

from lattice cimport *

cimport cython
import cython
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt, fabs
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

# compute gauss constraint (as an algebra element) at a certain lattice point x
cdef void gauss(double *links, double *electric, double *rho, long x, double* spacings, long *dims, long* acc,
                double g, double *r) nogil:
    cdef:
        long d, field_index, xs, field_index2
        double buffer0[3]
        double buffer1[3]
        double factor

    algebra_zero(r)
    for d in range(3):
        factor = 1.0 / (g * spacings[d] * spacings[d])
        field_index = get_field_index(x, d)
        buffer0[0] = electric[field_index+0]
        buffer0[1] = electric[field_index+1]
        buffer0[2] = electric[field_index+2]
        transport_neg(links, electric, x, d, dims, acc, buffer1)
        algebra_add(buffer0, buffer1, -1)
        algebra_add(r, buffer0, factor)

    algebra_add(r, &rho[3 * x], -1)

# compute gauss constraint (as an algebra element) at a certain lattice point x
cdef void de(double *links, double *electric, long x, double* spacings, long *dims, long* acc,
                double g, double *r) nogil:
    cdef:
        long d, field_index, xs, field_index2
        double buffer0[3]
        double buffer1[3]
        double factor

    algebra_zero(r)
    for d in range(3):
        factor = 1.0 / (g * spacings[d] * spacings[d])
        field_index = get_field_index(x, d)
        buffer0[0] = electric[field_index+0]
        buffer0[1] = electric[field_index+1]
        buffer0[2] = electric[field_index+2]
        transport_neg(links, electric, x, d, dims, acc, buffer1)
        algebra_add(buffer0, buffer1, -1)
        algebra_add(r, buffer0, factor)


# compute gauss constraint squared at a certain lattice point x
def gauss_sq(s, long x):
    cdef double abuffer[3]

    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double g = s.g

    gauss(&links0[0], &electric[0], &rho[0], x, &spacings[0], &dims[0], &acc[0], g, abuffer)
    return algebra_sq(abuffer)

# compute gauss constraint squared, averaged over the simulation box
def gauss_sq_total(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double g = s.g

    cdef:
        long ix, iy, iz, x
        double *buff
        double result = 0.0

    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    for ix in prange(nx0+1, nx1-1, nogil=True):
        buff = <double *>malloc(3 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                gauss(&links0[0], &electric[0], &rho[0], x, &spacings[0], &dims[0], &acc[0], g, buff)
                result += algebra_sq(buff)
        free(buff)

    return result / (dims[0] * dims[1] * dims[2])

# compute the gauss constraint at each lattice point and write it to an array
cdef void gauss_density(double *links, double *electric, double *rho,
                        double *spacings, long *dims, long *acc, long[::1] iter_dims, double g,
                        double *result):
    cdef long ix, iy, iz, x
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    for ix in prange(nx0+1, nx1-1, nogil=True):
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                gauss(&links[0], &electric[0], &rho[0], x, &spacings[0], &dims[0], &acc[0], g, &result[3*x])

# same as above, but only takes simulation object as argument
def compute_gauss_density(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double g = s.g

    # result
    cdef cnp.ndarray[double, ndim=1, mode="c"] result = np.zeros(3*s.N, dtype=np.double)

    cdef long ix, iy, iz, x
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    for ix in range(nx0+1, nx1-1):
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                gauss(&links0[0], &electric[0], &rho[0], x, &spacings[0], &dims[0], &acc[0], g, &result[3*x])

    return result

# computes gauss density squared, averaged over transverse plane
def compute_projected_gauss_density_sq(s):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double g = s.g

    # result
    cdef cnp.ndarray[double, ndim=1, mode="c"] result = np.zeros(s.dims[0]-2, dtype=np.double)

    cdef long ix, iy, iz, x
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    for ix in range(nx0+1, nx1-1):
        buff = <double *>malloc(3 * sizeof(double))
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                gauss(&links0[0], &electric[0], &rho[0], x, &spacings[0], &dims[0], &acc[0], g, buff)
                result[ix-2] += algebra_sq(buff)
        free(buff)
    result /= dims[1] * dims[2]
    return result

# relative gauss violation
def rel_gauss_violation(s):
        # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] current = s.j
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef double dt = s.dt
    cdef double g = s.g

    # result
    cdef double result = 0.0
    cdef double rho_sq = 0.0
    cdef double DE_sq = 0.0

    cdef long ix, iy, iz, x
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims
    cdef double *buff

    with nogil, parallel():
        buff = <double *>malloc(3 * sizeof(double))
        for ix in prange(nx0+1, nx1-1):
            for iy in range(ny0, ny1):
                for iz in range(nz0, nz1):
                    x = get_index_nm(ix, iy, iz, &dims[0])
                    de(&links0[0], &electric[0], x, &spacings[0], &dims[0], &acc[0], g, buff)
                    DE_sq += algebra_sq(buff)
                    rho_sq += algebra_sq(&rho[3*x])
        free(buff)
    result = sqrt(fabs(DE_sq - rho_sq) / rho_sq)
    return result, sqrt(DE_sq), sqrt(rho_sq)