"""
    Header file for commonly used functions
"""

cdef void mul2(double*a, double*b, long ta, long tb, double*r) nogil
cdef void mul2_fast(double*a, double*b, double*r) nogil
cdef void mul4(double*a, double*b, double*c, double*d, long ta, long tb, long tc, long td, double *r) nogil
cdef void mexp(double*a, double f, double*r) nogil
cdef void mlog(double*a, double*r) nogil
cdef void proj(double*u, double*r) nogil
cdef void act(double*u, double*a, long t, double*r) nogil

cdef void group_zero(double*g) nogil
cdef void group_unit(double*g) nogil
cdef void group_add(double*g0, double*g1, double f) nogil
cdef void group_set(double*g0, double*g1) nogil
cdef void group_hc(double*g0) nogil
cdef void algebra_zero(double*a) nogil
cdef void algebra_add(double*a0, double *a1, double f) nogil
cdef void algebra_mul(double*a, double f) nogil
cdef double algebra_sq(double*a) nogil
cdef double algebra_dot(double*a, double*b) nogil

cdef void plaq_pos(double*u, long x, long i, long j, long*dims, long*acc, double*r) nogil
cdef void plaq_neg(double*u, long x, long i, long j, long*dims, long*acc, double*r) nogil

cdef void transport_neg(double*links, double*field, long x, long d, long*dims, long*acc, double*r) nogil

cdef long get_index(long ix, long iy, long iz, long*dims) nogil
cdef long get_index_nm(long ix, long iy, long iz, long*dims) nogil
cdef void get_point(long x, long*dims, long*r) nogil
cdef long shift(long x, long i, long o, long*dims) nogil
cdef long shift2(long x, long i, long o, long*dims, long*acc) nogil
cdef long mod(long i, long n) nogil
cdef long get_field_index(long i, long d) nogil
cdef long get_group_index(long i, long d) nogil
