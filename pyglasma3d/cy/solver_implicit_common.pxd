cdef void leapfrog_plaquettes(long x, long d, double*links, long* neighbours, double*plaq_factor, double *result) nogil
cdef void implicit_plaquettes(double *u0, double *u1, double *u2, long x, long i, long *dims, long *acc, double *f, double *r) nogil

cdef void c(double *u, long x, long i, long j, long oi, long oj, long *dims, long *acc, double *r) nogil
cdef void m(double *up, double *uf, long x, long i, long j, long oi, long oj, long *dims, long *acc, double *r) nogil
cdef void w(double *u0, double *u1, double *u2, long x, long i, long oi, long *dims, long *acc, double *r) nogil
cdef void l(double *u, long x, long i, long j, long oi, long oj, long* dims, long* acc, double *r) nogil

cdef void link(long x, long i, long oi, long* dims, long* acc, long *xr, long *gr) nogil
cdef void u_bar(double *up, double *uf,long x, long i, long oi, long *dims, long *acc, double *r) nogil

cdef void tplaq(double *u0, double *u1, long x, long i, long oi, long *dims, long *acc, double *r) nogil

cdef void staples_a(long x, long i, long j, long oi, long oj, int mode, double *u0, double *u1, double *u2, long*dims, long*acc, double*r) nogil
cdef void staples_b(long x, long i, long j, long oi, long oj, double *u0, double *u1, double *u2, long*dims, long*acc, double*r) nogil
cdef void staples_c(long x, long i, long j, long oi, long oj, double *u0, double *u1, double *u2, long*dims, long*acc, double*r) nogil