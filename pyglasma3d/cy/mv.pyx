#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    McLerran-Venugopalan initial conditions
"""

import cython

import time

import solver_leapfrog as leapfrog
from lattice cimport *
from gauss cimport *
import numpy as np
cimport numpy as cnp
from cython.parallel import prange
from libc.math cimport sin, cos, exp, sqrt, erf, fabs, round
from cpython cimport bool
from libc.stdlib cimport malloc, free

import solver_implicit_common as common

cdef double PI = np.pi
cdef bool debug = False

# option for interpolation used in computing the Wilson line
# 0 - linear interpolation
# 1 - averaged quadratic interpolation
# 2 - constant interpolation

"""
    McLerran-Venugopalan initial conditions with coherent longitudinal structure and finite width
"""
def initialize_mv(s, double x0, double mu, double sigma, double mass, double uvt, long orientation):
    cdef:
        double a_l = s.a[0]
        double a_t = s.a[1]
        double dt = s.dt
        double g = s.g
        long NL = s.dims[0]
        long NT = s.dims[1]
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc

    # random 2D charge density
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi2d = np.zeros(3 * NT * NT, dtype=np.double)
    cdef long ic, x, y
    cdef double k2
    cdef double charge_factor = g * mu / a_t

    for ic in range(3):
        # generate random gaussian charges on a 2d plane
        field = np.zeros((NT, NT), dtype=np.complex128)
        field = np.random.normal(0, charge_factor, (NT, NT))

        # solve poisson equation in momentum space
        field = np.fft.fft2(field)
        for x in range(NT):
            for y in range(NT):
                k2 = k2_latt(x, y, NT, a_t)
                if (x > 0 or y > 0) and k2 <= uvt * uvt:
                    field[x, y] *= 1.0 / (k2 + mass * mass)
                else:
                    field[x, y] = 0.0
        field = np.fft.ifft2(field)

        # put results in phi2d array
        flat_field = np.real(field.flatten())
        phi2d[ic:3 * NT * NT:3] = np.real(flat_field[0:NT * NT:1])

    # compute Wilson line
    # at t = 0
    cdef cnp.ndarray[double, ndim=1, mode="c"] v = np.zeros(4 * s.N, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] u = s.u0
    compute_v(&phi2d[0], g, a_l, x0, sigma, orientation, &dims[0], &v[0])
    set_links(&v[0], &dims[0], &acc[0], &u[0])

    # at t = dt
    compute_v(&phi2d[0], g, a_l, x0 + orientation * dt, sigma, orientation, &dims[0], &v[0])
    u = s.u1
    set_links(&v[0], &dims[0], &acc[0], &u[0])

    # compute electric field from gauge links (inverse gauge link update)
    # make sure code works with old leapfrog and new implicit schemes
    if s.mode is None:
        leapfrog.evolve_u_inv(s)
    else:
        common.evolve_u_inv2(s)

    return v

"""
    McLerran-Venugopalan initial conditions with incoherent longitudinal structure (longitudinal randomness)
    and finite width.


"""

def initialize_mv_inc(s, double x0, double mu, double sigma, double kappa, double mass, double uvt, long orientation, int interpolation_mode=1):
    cdef:
        double a_l = s.a[0]
        double a_t = s.a[1]
        double dt = s.dt
        double g = s.g
        long NL = s.dims[0]
        long NT = s.dims[1]
        long i, x, y, z
        double t, clen

    """
        Translate parameters for consistency with 2D MV model
    """
    clen = kappa * sigma * sqrt(2.0)
    sigma /= sqrt(2.0)

    """
        Generating gauge field in covariant gauge
    """
    t = time.time()
    # generate 3d phi-field with transverse mass regulation
    cdef long n = 3 * NT * NT * NL
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi = np.zeros(n, dtype=np.double)

    # generate random charges according to 3D MV-model
    # covariance matrix
    cdef cnp.ndarray[double, ndim=1, mode="c"] mean = np.zeros(NL, dtype=np.double)
    cdef cnp.ndarray[double, ndim=2, mode="c"] cov = np.zeros((NL, NL), dtype=np.double)
    cdef long xi, yi
    for xi in prange(NL, nogil=True):
        for yi in range(NL):
            cov[xi, yi] = (g * mu / a_t) ** 2                           # variance
            cov[xi, yi] *= profile((xi - yi) * a_l, clen)               # correlation length
            cov[xi, yi] *= profile((xi + yi) * a_l * 0.5 - x0, sigma)   # longitudinal profile

    phi = np.moveaxis(np.random.multivariate_normal(mean, cov, 3 * NT * NT).flatten().reshape((NT, NT, 3, NL)),
                      -1, 0).flatten().copy()

    # compute Poisson kernel
    cdef cnp.ndarray[double, ndim=2, mode="c"] kernel = np.zeros((NT, NT / 2 + 1), dtype=np.double)
    cdef double k2
    for y in prange(NT, nogil=True):
        for z in range(NT / 2 + 1):
            k2 = k2_latt(y, z, NT, a_t)
            if (y > 0 or z > 0) and k2 <= uvt * uvt:
                kernel[y, z] = 1.0 / (k2 + mass * mass)

    # solve poisson for each transverse plane and color component
    phi = np.real(np.fft.irfft2(                                                # backwards (transverse) fft
        np.fft.rfft2(phi.reshape((NL, NT, NT, 3)), s=(NT, NT), axes=(1,2)) *    # (transverse) fft of charge density
        kernel[np.newaxis, :, :, np.newaxis],                                   # solve poisson with kernel
        axes=(1, 2)).flatten()).copy()                                          # flatten, take real and make contiguous

    """
        Compute Wilson line and initialize fields on the lattice
    """
    # compute V at t = -dt/2
    cdef cnp.ndarray[double, ndim=1, mode="c"] v = np.zeros(4 * NT * NT * NL, dtype=np.double)
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    v[0:4 * NT * NT * NL:4] = 1.0  # set all Vs to unit element
    cdef long ik, ix, iy, iz
    cdef long p0i, p1i, p2i, p3i
    cdef double*wbuffer1
    cdef double*wbuffer2
    cdef double z1, z2, z3
    cdef int m = 128

    for iy in prange(NT, nogil=True, schedule='guided'):
        wbuffer1 = <double *> malloc(4 * sizeof(double))
        wbuffer2 = <double *> malloc(4 * sizeof(double))
        for ik in range(NL):
            ix = ik if orientation == -1 else (NL - ik - 1)
            for iz in range(NT):
                p2i = get_index(ix, iy, iz, &dims[0])
                p1i = shift2(p2i, 0, +orientation, &dims[0], &acc[0])
                p0i = shift2(p1i, 0, +orientation, &dims[0], &acc[0])
                p3i = shift2(p2i, 0, -orientation, &dims[0], &acc[0])

                if interpolation_mode == 0:
                    compute_w_linear(- g * a_l, m, 1.0,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)

                elif interpolation_mode == 1:
                    compute_w_quadratic(- g * a_l, m, 1.0,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)

                elif interpolation_mode == 2:
                    compute_w_constant(- g * a_l, m, 1.0,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)

                mul2_fast(&v[4 * p1i], &wbuffer1[0], &wbuffer2[0])
                group_set(&v[4 * p2i], &wbuffer2[0])

        free(wbuffer1)
        free(wbuffer2)
    if debug:
        print("V at t=-dt/2", time.time() - t)
    t = time.time()

    cdef cnp.ndarray[double, ndim=1, mode="c"] u = s.u0
    set_links(&v[0], &dims[0], &acc[0], &u[0])
    if debug:
        print("set u0", time.time() - t)
    t = time.time()

    # compute V at t = dt/2
    for iy in prange(NT, nogil=True, schedule='guided'):
        wbuffer1 = <double *> malloc(4 * sizeof(double))
        wbuffer2 = <double *> malloc(4 * sizeof(double))
        for ik in range(NL):
            ix = ik
            for iz in range(NT):
                p1i = get_index(ix, iy, iz, &dims[0])
                p0i = shift2(p1i, 0, +orientation, &dims[0], &acc[0])
                p2i = shift2(p1i, 0, -orientation, &dims[0], &acc[0])
                p3i = shift2(p2i, 0, -orientation, &dims[0], &acc[0])

                if interpolation_mode == 0:
                    compute_w_linear(- g * a_l, m, dt / a_l,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)
                elif interpolation_mode == 1:
                    compute_w_quadratic(- g * a_l, m, dt / a_l,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)
                elif interpolation_mode == 2:
                    compute_w_constant(- g * a_l, m, dt / a_l,
                                        &phi[3 * p0i],
                                        &phi[3 * p1i],
                                        &phi[3 * p2i],
                                        &phi[3 * p3i],
                                        wbuffer1)

                mul2_fast(&v[4 * p1i], wbuffer1, wbuffer2)
                group_set(&v[4 * p1i], wbuffer2)

        free(wbuffer1)
        free(wbuffer2)
    if debug:
        print("V at t=+dt/2", time.time() - t)
    t = time.time()
    u = s.u1
    set_links(&v[0], &dims[0], &acc[0], &u[0])
    if debug:
        print("set u1", time.time() - t)
    t = time.time()

    # set links and compute electric field
    # make sure code works with old leapfrog and new implicit schemes
    if s.mode is None:
        leapfrog.evolve_u_inv(s)
    else:
        common.evolve_u_inv2(s)

    if debug:
        print("compute electric field", time.time() - t)
    t = time.time()

    # return Wilson line far behind nucleus
    return v

cdef double profile(double x, double s) nogil:
    return exp(-0.5*(x/s)**2) / (sqrt(2.0 * PI) * s)

"""
    McLerran-Venugopalan initial conditions with coherent longitudinal structure and finite width (perturbed)
"""
def initialize_mv_perturbed(s, double x0, double mu, double sigma, double mass, double uvt, long orientation,
                            double sigma_perturbed, double amplitude_perturbed, double shift_perturbed):
    cdef:
        double a_l = s.a[0]
        double a_t = s.a[1]
        double dt = s.dt
        double g = s.g
        long NL = s.dims[0]
        long NT = s.dims[1]
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc

    # random 2D charge density
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi2d = np.zeros(3 * NT * NT, dtype=np.double)
    cdef long ic, x, y
    cdef double k2
    cdef double charge_factor = g * mu / a_t

    for ic in range(3):
        # generate random gaussian charges on a 2d plane
        field = np.zeros((NT, NT), dtype=np.complex128)
        field = np.random.normal(0, charge_factor, (NT, NT))

        # solve poisson equation in momentum space
        field = np.fft.fft2(field)
        for x in range(NT):
            for y in range(NT):
                k2 = k2_latt(x, y, NT, a_t)
                if (x > 0 or y > 0) and k2 <= uvt * uvt:
                    field[x, y] *= 1.0 / (k2 + mass * mass)
                else:
                    field[x, y] = 0.0
        field = np.fft.ifft2(field)

        # put results in phi2d array
        flat_field = np.real(field.flatten())
        phi2d[ic:3 * NT * NT:3] = np.real(flat_field[0:NT * NT:1])

    # compute Wilson line
    # at t = 0
    cdef cnp.ndarray[double, ndim=1, mode="c"] v = np.zeros(4 * s.N, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] u = s.u0
    compute_v_perturbed(&phi2d[0], g, a_l, x0, sigma, orientation,
                        sigma_perturbed, amplitude_perturbed, shift_perturbed,
                        &dims[0], &v[0])
    set_links(&v[0], &dims[0], &acc[0], &u[0])

    # at t = dt
    compute_v_perturbed(&phi2d[0], g, a_l, x0 + orientation * dt, sigma, orientation,
                        sigma_perturbed, amplitude_perturbed, shift_perturbed,
                        &dims[0], &v[0])
    u = s.u1
    set_links(&v[0], &dims[0], &acc[0], &u[0])

    # compute electric field from gauge links (inverse gauge link update)
    # make sure code works with old leapfrog and new implicit schemes
    if s.mode is None:
        leapfrog.evolve_u_inv(s)
    else:
        common.evolve_u_inv2(s)

    return v


"""
    Set gauge links according to Wilson line
"""
cdef void set_links(double *v, long *dims, long*acc, double *u):
    cdef:
        long ix, iy, iz, d, x, xs, group_index
        double *group_buffer1
        double *group_buffer2

    for ix in prange(dims[0], nogil=True):
        group_buffer1 = <double *> malloc(4 * sizeof(double))
        group_buffer2 = <double *> malloc(4 * sizeof(double))
        for iy in range(dims[1]):
            for iz in range(dims[2]):
                x = get_index(ix, iy, iz, dims)
                for d in range(1, 3):
                    xs = shift2(x, d, 1, dims, acc)
                    group_index = get_group_index(x, d)
                    mul2(&v[4 * x], &v[4 * xs], 1, -1, group_buffer1)
                    mul2_fast(group_buffer1, &u[group_index], group_buffer2)
                    group_set(&u[group_index], group_buffer2)
        free(group_buffer1)
        free(group_buffer2)

"""
    Compute Wilson line for coherent MV model
"""
cdef void compute_v(double *phi2d,
                    double g, double a_l, double x0, double sigma, long orientation,
                    long *dims,
                    double *v):
    cdef:
        double *profile
        long ix, iy, iz, xt, x

    # set up profile
    cdef cnp.ndarray[double, ndim=1, mode="c"] profile_array = np.zeros(dims[0], dtype=np.double)
    profile = &profile_array[0]
    cdef double arg = 0.0
    for ix in range(dims[0]):
        arg = orientation * (x0 - ix * a_l) / (sqrt(2.0) * sigma)
        profile[ix] = - 0.5 * g * (1.0 + erf(arg))

    for iy in prange(dims[1], nogil=True):
        for iz in range(dims[2]):
            for ix in range(dims[0]):
                x = get_index(ix, iy, iz, dims)
                xt = dims[2] * iy + iz
                mexp(&phi2d[3 * xt], profile[ix], &v[4 * x])

"""
    Compute Wilson line for coherent MV model with perturbation
"""
cdef void compute_v_perturbed(double *phi2d,
                              double g, double a_l, double x0, double sigma, long orientation,
                              double sigma_perturbed, double amplitude_perturbed, double shift_perturbed,
                              long *dims,
                              double *v):
    cdef:
        double *profile
        long ix, iy, iz, xt, x

    # set up profile
    cdef cnp.ndarray[double, ndim=1, mode="c"] profile_array = np.zeros(dims[0], dtype=np.double)
    profile = &profile_array[0]
    cdef double arg = 0.0
    for ix in range(dims[0]):
        arg = orientation * (x0 - ix * a_l) / (sqrt(2.0) * sigma)
        profile[ix] = - 0.5 * g * (1.0 + erf(arg))
        # add perturbation
        arg = orientation * (x0 - ix * a_l + shift_perturbed) / (sqrt(2.0) * sigma_perturbed)
        profile[ix] += - 0.5 * g * (1.0 + erf(arg)) * amplitude_perturbed


    for iy in prange(dims[1], nogil=True):
        for iz in range(dims[2]):
            for ix in range(dims[0]):
                x = get_index(ix, iy, iz, dims)
                xt = dims[2] * iy + iz
                mexp(&phi2d[3 * xt], profile[ix], &v[4 * x])

"""
    Compute (parts of the) Wilson line for incoherent MV model (linear interpolation)
"""
cdef void compute_w(double z1, double z2, int m, double*p1, double*p2, double a_l, double g, double*r) nogil:
    cdef double factor = - g * fabs(z2 - z1) * a_l / m
    cdef double[3] p
    cdef double[4] w, wbuffer, wbuffer2
    cdef double u
    cdef int i, mi

    group_unit(&w[0])
    for mi in range(0, m):
        u = (mi + 0.0) / m
        for i in range(3):
            p[i] = p1[i] + (p2[i] - p1[i]) * u
        mexp(&p[0], factor, &wbuffer[0])
        mul2_fast(&w[0], &wbuffer[0], &wbuffer2[0])
        group_set(&w[0], &wbuffer2[0])
    group_set(&r[0], &w[0])

"""
    Compute (parts of the) Wilson line for incoherent MV model (quadratic interpolation)
"""
cdef void compute_w_quadratic(double factor, int m, double umax, double*p0, double*p1, double *p2, double *p3, double*r) nogil:
    cdef double[3] p
    cdef double[4] w, wbuffer, wbuffer2
    cdef double u
    cdef int i, mi, mmax

    group_unit(&w[0])
    mmax = int(umax * m)
    factor /= m
    for mi in range(0, mmax):
        u = mi / (1.0 * m)
        for i in range(3):
            p[i] = 4 * p1[i]
            p[i] -= (p0[i] + 3 * p1[i] - 5 * p2[i] + p3[i]) * u
            p[i] += (p0[i] - p1[i] - p2[i] + p3[i]) * u * u
            p[i] *= 0.25
        mexp(&p[0], factor, &wbuffer[0])
        mul2_fast(&w[0], &wbuffer[0], &wbuffer2[0])
        group_set(&w[0], &wbuffer2[0])
    group_set(&r[0], &w[0])

"""
    Compute (parts of the) Wilson line for incoherent MV model (linear interpolation)
"""
cdef void compute_w_linear(double factor, int m, double umax, double*p0, double*p1, double *p2, double *p3, double*r) nogil:
    cdef double[3] p
    cdef double[4] w, wbuffer, wbuffer2
    cdef double u
    cdef int i, mi, mmax

    group_unit(&w[0])
    mmax = int(umax * m)
    factor /= m
    for mi in range(0, mmax):
        u = mi / (1.0 * m)
        for i in range(3):
            p[i] = p1[i] + (p2[i] - p1[i]) * u
        mexp(&p[0], factor, &wbuffer[0])
        mul2_fast(&w[0], &wbuffer[0], &wbuffer2[0])
        group_set(&w[0], &wbuffer2[0])
    group_set(&r[0], &w[0])

"""
    Compute (parts of the) Wilson line for incoherent MV model (constant approximation)
    This makes sense for the implicit solver because ax = dt. There is no ambiguity in defining the Wilson line.
"""
cdef void compute_w_constant(double factor, int m, double umax, double*p0, double*p1, double *p2, double *p3, double*r) nogil:
    mexp(&p1[0], factor, &r[0])

"""
    Effective transverse lattice momentum squared
"""
cdef inline double k2_latt(long x, long y, long nt, double a_t) nogil:
    cdef double result = 4.0
    result -= 2.0 * cos((2.0 * PI * x) / nt)
    result -= 2.0 * cos((2.0 * PI * y) / nt)
    return result / (a_t * a_t)

"""
    Effective longitudinal lattice momentum squared
"""
cdef inline double k2_latt_long(long z, long nl, double a_l):
    return (2.0 - 2.0 * cos((2.0 * PI * z) / nl)) / (a_l * a_l)
