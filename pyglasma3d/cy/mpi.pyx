#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
"""
    This class provides a wrapper for the Simulation object with some extra functions for MPI communication.
"""

from __future__ import print_function

from pyglasma3d.core import Simulation, Observables
import numpy as np
import time

import mpi4py

mpi4py.rc.recv_mprobe = False
from mpi4py import MPI


class MPISimulation:
    def __init__(self, dimensions, spacings, time_step, coupling, node_config):
        iter_dims = [1, dimensions[0] - 1, 0, dimensions[1], 0, dimensions[2]]
        self.s = Simulation(dimensions, iter_dims, spacings, time_step, coupling)
        self.node_config = node_config
        self.node_type = None

        self.comm = MPI.COMM_WORLD
        self.rank = self.comm.Get_rank()
        self.num = self.comm.Get_size()
        self.debug = False
        self.write_counter = 0

        if self.node_config['num'] != self.num:
            self.debug = True
            self.log("Fatal error: incorrect number of nodes!",
                     "Expected:", self.node_config['num'],
                     "Got:", self.num)
            import sys
            sys.exit()

    def log(self, *message):
        if self.debug:
            outputstr = "[r={0}, t={1}] MPI: ".format(self.rank, self.s.t)
            for m in message:
                outputstr += str(m) + " "
            print(outputstr)

    def init(self):
        self.log("Version:", MPI.Get_version())
        self.log("Vendor:", MPI.get_vendor())
        self.log("Synchronizing nodes...")

        # identify type according to node_config
        r = self.rank
        num = self.num
        nc = self.node_config
        nx = self.s.dims[0]
        ny = self.s.dims[1]
        nz = self.s.dims[2]
        n_plane = 4 * 3 * ny * nz

        if nc['left'] < r < nc['right']:
            self.node_type = 'm'
        elif r < nc['left']:
            self.node_type = 'l'
        elif r > nc['right']:
            self.node_type = 'r'
        elif r == nc['left']:
            self.node_type = 'nl'
        elif r == nc['right']:
            self.node_type = 'nr'
        # nucleus nodes send messages to nodes behind them

        if self.node_type == 'nl':
            # boundary data
            n0 = 0
            n1 = 4 * 3 * ny * nz
            ub = self.s.u0[n0:n1]
            for d in range(0, r):
                self.log("Sending boundary data to rank " + str(self.rank))
                self.comm.send(ub, dest=d, tag=100)

        if self.node_type == 'nr':
            # boundary data
            n0 = 4 * 3 * (nx - 1) * ny * nz
            n1 = 4 * 3 * nx * ny * nz
            ub = self.s.u0[n0:n1]
            for d in range(r + 1, num):
                self.log("Sending boundary data to rank " + str(self.rank))
                self.comm.send(ub, dest=d, tag=101)

        # nodes behind nuclei receive boundary data
        ub = None
        if self.node_type == 'l':
            ub = self.comm.recv(source=nc['left'], tag=100)
            self.log("Received boundary data form left nucleus.")

        if self.node_type == 'r':
            ub = self.comm.recv(source=nc['right'], tag=101)
            self.log("Received boundary data form right nucleus.")

        # set fields according to boundary data
        if ub is not None:
            for ix in range(0, nx):
                self.log("Writing boundary data.")
                self.s.u0[4 * 3 * ny * nz * ix: 4 * 3 * ny * nz * (ix + 1)] = ub[0:4 * 3 * ny * nz]
                self.s.u1[4 * 3 * ny * nz * ix: 4 * 3 * ny * nz * (ix + 1)] = ub[0:4 * 3 * ny * nz]

        self.log("Synchronization is complete.")

    def evolve(self):
        t = time.time()
        # evolve local simulation object
        self.s.evolve()
        self.log("local evolution complete", time.time() - t)

        t = time.time()

        # some definitions

        nx = self.s.dims[0]
        ny = self.s.dims[1]
        nz = self.s.dims[2]
        n = 3 * ny * nz
        r = self.rank
        num = self.num
        c = self.comm
        s = self.s
        nshift = (nx - 2) * ny * nz
        reqs = []

        # send and receive ghost cells
        # sending cells at ix = 1 and ix = nx-2
        ixb = 1
        ixe = nx - 2

        if r > 0:
            reqs.append(c.Isend([s.e[3 * ixb * n: 3 * (ixb + 1) * n], MPI.DOUBLE], dest=r - 1, tag=1))
            reqs.append(c.Isend([s.u0[4 * ixb * n: 4 * (ixb + 1) * n], MPI.DOUBLE], dest=r - 1, tag=2))
            reqs.append(c.Isend([s.u1[4 * ixb * n: 4 * (ixb + 1) * n], MPI.DOUBLE], dest=r - 1, tag=3))
        if r < num - 1:
            reqs.append(c.Isend([s.e[3 * ixe * n: 3 * (ixe + 1) * n], MPI.DOUBLE], dest=r + 1, tag=4))
            reqs.append(c.Isend([s.u0[4 * ixe * n: 4 * (ixe + 1) * n], MPI.DOUBLE], dest=r + 1, tag=5))
            reqs.append(c.Isend([s.u1[4 * ixe * n: 4 * (ixe + 1) * n], MPI.DOUBLE], dest=r + 1, tag=6))

        # receiving cells at ix = 0 and ix = nx-1
        ixb = 0
        ixe = nx - 1

        if r > 0:
            reqs.append(c.Irecv([s.e[3 * ixb * n: 3 * (ixb + 1) * n], MPI.DOUBLE], source=r - 1, tag=4))
            reqs.append(c.Irecv([s.u0[4 * ixb * n: 4 * (ixb + 1) * n], MPI.DOUBLE], source=r - 1, tag=5))
            reqs.append(c.Irecv([s.u1[4 * ixb * n: 4 * (ixb + 1) * n], MPI.DOUBLE], source=r - 1, tag=6))
        if r < num - 1:
            reqs.append(c.Irecv([s.e[3 * ixe * n: 3 * (ixe + 1) * n], MPI.DOUBLE], source=r + 1, tag=1))
            reqs.append(c.Irecv([s.u0[4 * ixe * n: 4 * (ixe + 1) * n], MPI.DOUBLE], source=r + 1, tag=2))
            reqs.append(c.Irecv([s.u1[4 * ixe * n: 4 * (ixe + 1) * n], MPI.DOUBLE], source=r + 1, tag=3))

        self.log("Ghost cell isend/irecv set up.", time.time() - t)
        t = time.time()

        # collect inactive charge planes
        pl_to_right = None
        pl_to_left = None
        send_to_left = False
        send_to_right = False
        for pl in s.charges:
            if pl.active is False:
                if pl.orientation > 0 and r < num - 1:
                    send_to_right = True
                    pl_to_right = pl

                if pl.orientation < 0 and r > 0:
                    send_to_left = True
                    pl_to_left = pl

                s.charges.remove(pl)

        # inform neighbours of incoming charge plane
        if r < num - 1:
            reqs.append(c.isend(send_to_right, dest=r + 1, tag=11))
        if r > 0:
            reqs.append(c.isend(send_to_left, dest=r - 1, tag=12))

        # send charge planes to neighbour
        if pl_to_right is not None:
            self.log("Sending charge plane to right node. (isend)")
            reqs.append(c.isend(pl_to_right, dest=r + 1, tag=13))
        if pl_to_left is not None:
            self.log("Sending charge plane to left node. (isend)")
            reqs.append(c.isend(pl_to_left, dest=r - 1, tag=14))

        if r < num - 1:
            self.log("Waiting for charge plane from right. (recv)")
            if c.recv(source=r + 1, tag=12):
                pl_from_right = c.recv(source=r + 1, tag=14)
                pl_from_right.index_offset += nshift
                pl_from_right.active = True
                s.charges.append(pl_from_right)

        if r > 0:
            self.log("Waiting for charge plane from left. (recv)")
            if c.recv(source=r - 1, tag=11):
                pl_from_left = c.recv(source=r - 1, tag=13)
                pl_from_left.index_offset -= nshift
                pl_from_left.active = True
                s.charges.append(pl_from_left)

        # wait for all requests to finish
        MPI.Request.Waitall(requests=reqs)

        diff = time.time() - t
        self.log("Charge planes complete. ", diff)

    def write_energy_momentum(self, max_writes, file_path):
        # rank 0 is 'master'
        if self.rank > 0:
            self.log("Sending T to rank 0.")
            # compute Tmunu and send data to rank 0
            EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(self.s)
            data = np.concatenate((EL, BL, ET, BT, SL))
            self.comm.Send([data, MPI.DOUBLE], dest=0, tag=90)
            self.log("Sent T to rank 0!")
        else:
            # receive data from other nodes and write to file
            reqs = []
            buffs = []
            for r in range(1, self.num):
                nx = self.s.NL - 2
                buff = np.zeros(5 * nx, dtype=np.double)
                reqs.append(self.comm.Irecv([buff, MPI.DOUBLE], source=r, tag=90))
                buffs.append(buff)

            # compute Tmunu
            EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(self.s)
            nx = self.s.NL - 2
            EL = np.resize(EL, self.num * nx)
            BL = np.resize(BL, self.num * nx)
            ET = np.resize(ET, self.num * nx)
            BT = np.resize(BT, self.num * nx)
            SL = np.resize(SL, self.num * nx)

            # wait for requests to finish
            self.log("Waiting for T data.")
            statuses = [MPI.Status() for r in range(1, self.num)]
            try:
                MPI.Request.Waitall(requests=reqs, statuses=statuses)
            except MPI.Exception as e:
                self.log("Error receiving T data!", e.Get_error_string())
                for s in statuses:
                    self.log("STATUS:", s.Get_error())
                return

            self.log("Received T from all ranks.")

            # combine data into big arrays
            nx = self.s.NL - 2
            for r in range(1, self.num):
                buff = buffs[r - 1]
                EL[r * nx: (r + 1) * nx] = buff[0 * nx: 1 * nx]
                BL[r * nx: (r + 1) * nx] = buff[1 * nx: 2 * nx]
                ET[r * nx: (r + 1) * nx] = buff[2 * nx: 3 * nx]
                BT[r * nx: (r + 1) * nx] = buff[3 * nx: 4 * nx]
                SL[r * nx: (r + 1) * nx] = buff[4 * nx: 5 * nx]

            def toint(i):
                return np.array([i], dtype=np.int64).tobytes()

            # open file
            import os
            dirname = os.path.dirname(file_path)
            if not os.path.exists(file_path):
                if not os.path.exists(dirname):
                    os.makedirs(dirname)

            # write header
            f = None
            if self.write_counter == 0:
                f = open(file_path, mode='w+')
                f.write(toint(nx * self.num))
                f.write(toint(max_writes))
            elif self.write_counter < max_writes:
                f = open(file_path, mode='a+')
            else:
                self.log("Error: Trying to write too much T data!")
                return

            # write body
            if f is not None:
                f.write(EL.tobytes())
                f.write(BL.tobytes())
                f.write(ET.tobytes())
                f.write(BT.tobytes())
                f.write(SL.tobytes())
                f.close()
                self.write_counter += 1
                self.log("Writing T data complete.")
            else:
                self.log("Error: Can't open file!")

            return
