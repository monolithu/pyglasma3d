#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A module for computing Wilson line related stuff (correlators)
"""

from lattice cimport *
cimport cython
import cython
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt, acos, round
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free
import scipy.optimize as opt


"""
    Computes the coordinate Wilson line correlator in the adjoint representation
"""
def compute_correlator(double[::1] v, long nt):
    # assumption: v is Wilson line at boundary plane behind nucleus
    cdef cnp.ndarray[double, ndim=1, mode="c"] c_2d = np.zeros(nt * nt, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] c_1d = np.zeros(nt / 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] counter_2d = np.zeros(nt * nt, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] counter_1d = np.zeros(nt / 2, dtype=np.double)
    cdef double* group_buffer
    cdef double c_fund
    cdef double c_adj
    cdef long ix1, iy1, ix2, iy2, x1, x2
    cdef long ix, iy, x
    cdef double dist
    cdef long idist
    for ix1 in prange(nt, nogil=True):
        group_buffer = <double *>malloc(4 * sizeof(double))
        for iy1 in range(nt):
            x1 = nt * ix1 + iy1
            for ix2 in range(nt):
                for iy2 in range(nt):
                    x2 = nt * ix2 + iy2
                    ix = mod(ix1 - ix2, nt)
                    iy = mod(iy1 - iy2, nt)
                    x = nt * ix + iy

                    mul2(&v[4*x1], &v[4*x2], 1, -1, &group_buffer[0])

                    # tr(V(x)V(y)^t) in fundamental rep
                    c_fund = 2.0 * group_buffer[0]

                    # tr(V(x)V(y^t)) in adjoint rep (see arXiv:0711.3039, eq. (8))
                    c_adj = c_fund * c_fund - 1.0

                    c_2d[x] += c_adj
                    counter_2d[x] += 1.0

                    dist = sqrt((ix1 - ix2) * (ix1 - ix2) + (iy1 - iy2) * (iy1 - iy2))
                    idist = <long> round(dist)
                    if idist < nt/2:
                        c_1d[idist] += c_adj
                        counter_1d[idist] += 1.0
        free(group_buffer)

    c_2d /= counter_2d
    c_1d /= counter_1d

    return c_2d, c_1d

"""
    Computes Qs_coord from the 1d coordinate Wilson line correlation function
"""
def compute_qs_coord(double[::1] c_1d, long nt):
    # find Qs_coord with interpolation and newton method (see arXiv:0711.3039, p. 6, IP-Sat definition)
    x_list = np.linspace(0.0, nt/2-1, num=nt/2)
    y_list = c_1d

    def interp(x):
        return np.interp(x, x_list, y_list) - 3.0*np.exp(-0.25)

    inv_qs = opt.bisect(interp, 0, nt/4)
    return 1.0 / inv_qs


"""
    Computes the 1d Wilson line correlation function in momentum space times k^2
    and returns the array and the maximum value (Qs via momentum space definition)
"""
def compute_c_1d_momentum(c_2d, long nt, double at):
    c_2d_array = np.array(c_2d).copy()
    c_2d_array = np.fft.rfft2(c_2d_array)
    cdef long n_bins = nt / 8
    cdef cnp.ndarray[double, ndim=1, mode="c"] corr_1d = np.zeros(n_bins, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] counter = np.zeros(n_bins, dtype=np.double)

    cdef long ix, iy, ik
    cdef double k2, k2_max
    k_max = 2.0 / at
    for ix in range(nt):
        for iy in range(nt/2):
            k2 = k2_latt(ix, iy, nt, at)
            ik = <long> (sqrt(k2) / k_max * n_bins)
            c_2d_array[ix, iy] *= k2
            if ik < n_bins:
                corr_1d[ik] += np.abs(c_2d_array[ix, iy])
                counter[ik] += 1.0

    max_arg = np.argmax(corr_1d) * k_max / (1.0 * n_bins)

    return corr_1d / counter, max_arg


cdef inline double k2_latt(long x, long y, long nt, double a_t):
    cdef double result = 4.0
    result -= 2.0 * cos((2.0 * np.pi * x) / nt)
    result -= 2.0 * cos((2.0 * np.pi * y) / nt)
    return result / (a_t * a_t)
