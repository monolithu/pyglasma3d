#cython: boundscheck=False, wraparound=False, nonecheck=False
"""
    Gauge transformations
"""
from lattice cimport *
from pyglasma3d.core import Simulation

cimport cython
import cython
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

def apply_gauge(s, gauge_tr):
    cdef:
        long n = (<long> s.dims[0]) * (<long> s.dims[1]) * (<long> s.dims[2])
        long i, j, x, xs
        cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
        cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
        cnp.ndarray[double, ndim=1, mode="c"] gauge = gauge_tr

    cdef:
        double *buffer

    buffer = <double *>malloc(4 * sizeof(double))
    with nogil:
        for x in range(n):
            for i in range(3):
                act(&gauge[4*x], &electric[3*(3*x+i)], 1, &electric[3*(3*x+i)])
                xs = shift2(x, i, 1, &dims[0], &acc[0])
                mul2(&gauge[4*x], &links0[4*(3*x+i)], -1, 1, buffer)
                mul2(buffer, &gauge[4*xs], 1, 1, &links0[4*(3*x+i)])
                mul2(&gauge[4*x], &links1[4*(3*x+i)], -1, 1, buffer)
                mul2_fast(buffer, &gauge[4*xs], &links1[4*(3*x+i)])
    free(buffer)



