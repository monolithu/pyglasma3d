#cython: boundscheck=False, wraparound=False, nonecheck=False
"""
    Coulomb gauge
"""
from lattice cimport *
from pyglasma3d.core import Simulation

cimport cython
import cython
import numpy as np
import scipy.fftpack
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

cdef double PI = np.pi

"""
    Removes pure-gauge trails behind nuclei using Wilson line v
"""

def remove_gauge_trails(s, v, long boundary, long dnx):
    cdef:
        long nx, ny, nz, nx0, nx1
        long i, j, x, xs, ix, iy, iz
        cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
        cnp.ndarray[double, ndim=1, mode="c"] w = v
        cnp.ndarray[double, ndim=1, mode="c"] alpha

    nx, ny, nz = s.dims

    # save algebra field associated with Wilson line
    alpha = np.zeros(3 * ny * nz, dtype=np.double)
    cdef long index_shift
    if boundary == 0:
        index_shift = 0
        nx0 = 0
        nx1 = dnx
    else:
        index_shift = (nx - 1) * ny * nz
        nx0 = nx - dnx
        nx1 = nx

    for x in prange(ny * nz, nogil=True):
        mlog(&w[4*(x+index_shift)], &alpha[3*x])

    # create gauge transformation
    cdef double factor, dx
    cdef cnp.ndarray[double, ndim=1, mode="c"] gtr = np.zeros(4 * nx * ny * nz, dtype=np.double)
    for ix in range(nx):
        if nx0 <= ix <= nx1:
            dx = <double> (nx1 - (ix - nx0))
            factor = dx / (1.0 * dnx)
            for iy in range(ny):
                for iz in range(nz):
                    i = iy * ny + iz
                    x = get_index(ix, iy, iz, &dims[0])
                    mexp(&alpha[3*i], factor, &gtr[4*x])
                    #group_set(&gtr[4*x], &w[4*x])
        else:
            for i in range(ny * nz):
                x = ny * nz * ix + i
                group_unit(&gtr[4*x])

    apply_gauge_tr(s, gtr)


"""
    Coulomb gauge iteration
"""

def apply_coulomb_gauge(s):
    cdef:
        long nx, ny, nz, n
        long i, j, x, xs, ix, iy, iz
        cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
        cnp.ndarray[double, ndim=1, mode="c"] a = s.a
        double g = s.g
        cnp.ndarray[double, ndim=1, mode="c"] delta

    nx, ny, nz = s.dims
    n = nx * ny * nz

    delta = np.zeros(3 * n, dtype=np.double)
    compute_delta(&links0[0], &dims[0], &acc[0], &a[0], g, &delta[0])
    cdef double theta = compute_theta(&delta[0], n)

    cdef cnp.ndarray[double, ndim=4, mode="c"] inverse_laplace = np.zeros((nx, ny, nz/2+1, 3))
    for ix in range(nx):
        for iy in range(ny):
            for iz in range(nz/2+1):
                if ix > 0 or iy > 0 or iz > 0:
                    inverse_laplace[ix, iy, iz, 0:3] = 6.0 / k2_latt(ix, iy, iz, &dims[0], &a[0])

    cdef cnp.ndarray[double, ndim=1, mode="c"] gtr = np.zeros(4*n, dtype=np.double)

    cdef long max_iters = 200
    cdef long it = 0
    cdef double theta_min = 10e-18
    cdef double alpha = 0.05

    while theta > theta_min and it < max_iters:
        compute_delta(&links0[0], &dims[0], &acc[0], &a[0], g, &delta[0])
        theta = compute_theta(&delta[0], n)
        fft_data = np.reshape(delta, (nx, ny, nz, 3))
        fft_data = np.fft.rfft2(fft_data, axes=[1,2])
        fft_data = scipy.fftpack.dct(fft_data, axis=0, type=2, norm='ortho')
        fft_data *= inverse_laplace
        fft_data = scipy.fftpack.idct(fft_data, axis=0, type=2, norm='ortho')
        fft_data = np.fft.irfft2(fft_data, axes=[1,2])
        delta = np.real(fft_data.flatten())

        for x in prange(n, nogil=True, schedule="guided"):
            mexp(&delta[3*x], g * alpha, &gtr[4*x])

        apply_gauge_tr(s, gtr)
        it += 1

    print("Coulomb gauge complete after " + str(it) + " iterations.")
    return theta


cdef void compute_delta(double* links, long* dims, long* acc, double* a, double g, double* delta):
    cdef long n = dims[0] * dims[1] * dims[2]
    cdef long x, xs, d, gi1, gi2, ix, iy, iz
    cdef double* pbuffer
    cdef double factor

    for x in prange(n, nogil=True, schedule="guided"):
        pbuffer = <double *>malloc(3 * sizeof(double))
        algebra_zero(&delta[3*x])
        for d in range(3):
            factor = 1.0 / (g * a[d] * a[d])
            gi1 = get_group_index(x, d)
            xs = shift2(x, d, -1, dims, acc)
            gi2 = get_group_index(xs, d)
            proj(&links[gi1], pbuffer)
            algebra_add(&delta[3*x], &pbuffer[0], factor)
            proj(&links[gi2], pbuffer)
            algebra_add(&delta[3*x], &pbuffer[0], -factor)


cdef double compute_theta(double* delta, long n):
    cdef double theta = 0.0
    for x in range(n):
        theta += algebra_sq(&delta[3*x])
    return theta / n


cdef inline double k2_latt(long ix, long iy, long iz, long* dims, double* a):
    cdef double result = 0.0
    result += 2.0 * (1.0 - cos((2.0 * PI * ix) / (2 * dims[0])))
    result += 2.0 * (1.0 - cos((2.0 * PI * iy) / dims[1]))
    result += 2.0 * (1.0 - cos((2.0 * PI * iz) / dims[2]))
    return result


def apply_gauge_tr(s, gauge_tr):
    cdef:
        long nx, ny, nz, n
        long i, j, x, xs, ix, iy, iz
        cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
        cnp.ndarray[double, ndim=1, mode="c"] chargedensity = s.r
        cnp.ndarray[double, ndim=1, mode="c"] currentdensity = s.j
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
        cnp.ndarray[double, ndim=1, mode="c"] gauge = gauge_tr
        double* gbuffer

    nx, ny, nz = s.dims
    n = nx * ny * nz

    # standard transformation for bulk of simulation box
    for ix in prange(nx-1, nogil=True, schedule="guided"):
        gbuffer = <double *>malloc(4 * sizeof(double))
        for iy in range(ny):
            for iz in range(nz):
                x = get_index_nm(ix, iy, iz, &dims[0])
                for i in range(3):
                    act(&gauge[4*x], &electric[3*(3*x+i)], 1, &electric[3*(3*x+i)])
                    xs = shift2(x, i, 1, &dims[0], &acc[0])
                    mul2(&gauge[4*x], &links0[4*(3*x+i)], -1, 1, gbuffer)
                    mul2(gbuffer, &gauge[4*xs], 1, 1, &links0[4*(3*x+i)])
                    mul2(&gauge[4*x], &links1[4*(3*x+i)], -1, 1, gbuffer)
                    mul2_fast(gbuffer, &gauge[4*xs], &links1[4*(3*x+i)])
        free(gbuffer)

    # treat right longitudinal boundary differently
    for iy in prange(ny, nogil=True, schedule="guided"):
        gbuffer = <double *>malloc(4 * sizeof(double))
        for iz in range(nz):
            x = get_index_nm(nx-1, iy, iz, &dims[0])
            for i in range(1, 3):
                act(&gauge[4*x], &electric[3*(3*x+i)], 1, &electric[3*(3*x+i)])
                xs = shift2(x, i, 1, &dims[0], &acc[0])
                mul2(&gauge[4*x], &links0[4*(3*x+i)], -1, 1, gbuffer)
                mul2(gbuffer, &gauge[4*xs], 1, 1, &links0[4*(3*x+i)])
                mul2(&gauge[4*x], &links1[4*(3*x+i)], -1, 1, gbuffer)
                mul2_fast(gbuffer, &gauge[4*xs], &links1[4*(3*x+i)])
        free(gbuffer)

    # apply gauge transformation to charge planes
    cdef long index_offset, num, xt
    cdef cnp.ndarray[double, ndim=1, mode="c"] charges
    for pl in s.charges:
        index_offset = pl.index_offset
        charges = pl.q
        num = ny * nz
        for xt in prange(num, nogil=True, schedule="guided"):
            x = index_offset + xt
            act(&gauge[4*x], &charges[3*xt], 1, &charges[3*xt])

    # apply gauge transformation to charge and current density on the lattice
    for ix in prange(nx, nogil=True, schedule="guided"):
        gbuffer = <double *>malloc(4 * sizeof(double))
        for iy in range(ny):
            for iz in range(nz):
                x = get_index_nm(ix, iy, iz, &dims[0])
                act(&gauge[4*x], &chargedensity[3*x], 1, &chargedensity[3*x])
                act(&gauge[4*x], &currentdensity[3*x], 1, &currentdensity[3*x])
        free(gbuffer)