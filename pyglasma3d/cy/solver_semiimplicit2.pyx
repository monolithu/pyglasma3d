#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    A module implementing the consistent implicit solver for lattice Yang-Mills, which can be derived from an action
    principle and conserves a modified Gauss constraint.

    We use a damped iterative approach to solving the implicit equations.
"""
cimport lattice as la

cimport solver_implicit_common as ccommon
import solver_implicit_common as common

from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
import numpy as np
cimport numpy as cnp
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt
from cpython cimport bool
from time import time

cimport openmp
#openmp.omp_set_num_threads(1)

def evolve_iterate(s, iterations, damping):
    # iteration for stabilization
    combined_evolution(s, iterations, damping)

def evolve_initial(s):
    # initial guess from leapfrog
    common.evolve_initial(s)

def combined_evolution(s, int iterations, double damping):
    # convert python objects to cython things
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.up
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u2 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e0 = s.ep
    cdef cnp.ndarray[double, ndim=1, mode="c"] e1 = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] j = s.j
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[long, ndim=1, mode="c"] iter_dims = s.iter_dims
    cdef cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
    cdef cnp.ndarray[long, ndim=1, mode="c"] neighbours = s.staple_neighbours
    cdef double dt = s.dt
    cdef double alpha = damping

    cdef long ix, iy, iz, di, dj, cell_index, field_index, group_index, k

    # temporary buffers
    cdef double *group_buffer
    cdef double *algebra_buffer

    # need to turn iteration bounds to integers due to nogil, otherwise the compiler crashes
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    cdef double current_factor = - s.g * spacings[0] * dt
    cdef long num_transverse = dims[1] * dims[2]
    cdef int current_iteration

    cdef double plaq_factor[3]
    for dj in range(3):
        plaq_factor[dj] = dt / spacings[dj] ** 2

    for current_iteration in range(iterations):
        # evolve electric field
        with nogil, parallel():
            for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
                algebra_buffer = <double *> malloc(3 * sizeof(double))
                for di in range(3):
                    la.algebra_zero(algebra_buffer)

                    field_index = la.get_field_index(cell_index, di)
                    group_index = la.get_group_index(cell_index, di)

                    # standard leapfrog plaquettes
                    compute_staple_sum(cell_index, di, &u1[0], &dims[0], &acc[0], &plaq_factor[0], &algebra_buffer[0])

                    # correction terms
                    first_correction_eom(cell_index, di, &dims[0], &acc[0], &spacings[0], dt, &u0[0], &u1[0], &u2[0], &algebra_buffer[0])
                    if di > 0:
                        second_correction_eom(cell_index, di, &dims[0], &acc[0], &spacings[0], dt, &u0[0], &u1[0], &u2[0], &algebra_buffer[0])

                    # damped iterative solver
                    la.algebra_mul(&e1[field_index], alpha)
                    la.algebra_add(&e1[field_index], &e0[field_index], 1.0 - alpha)
                    la.algebra_add(&e1[field_index], algebra_buffer, 1.0 - alpha)

                la.algebra_add(&e1[la.get_field_index(cell_index, 0)], &j[3 * cell_index], (1.0 - alpha) * current_factor)
                free(algebra_buffer)

        # evolve gauge links
        with nogil, parallel():
            for cell_index in prange(nx0 * num_transverse, nx1 * num_transverse, schedule='guided'):
                group_buffer = <double *> malloc(4 * sizeof(double))
                for di in range(3):
                    field_index = la.get_field_index(cell_index, di)
                    group_index = la.get_group_index(cell_index, di)

                    for k in range(3):
                        group_buffer[k+1] = - 0.5 * dt * e1[field_index+k]

                    # unitarize
                    group_buffer[0] = 0.25 * la.algebra_sq(&e1[field_index]) * dt ** 2
                    group_buffer[0] = sqrt(1.0 - group_buffer[0])

                    la.mul2_fast(group_buffer, &u1[group_index], &u2[group_index])
                free(group_buffer)

# Gauss constraint, averaged across the lattice
def gauss_constraint(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] g_density
    g_density = gauss_density(s)
    cdef double result = 0.0
    cdef long n = s.N
    cdef long x

    with nogil:
        for x in prange(n):
            result += la.algebra_sq(&g_density[3*x])

    return result / n

# Gauss constraint violation for paper
def gauss_constraint_violation(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] rho = s.r
    cdef cnp.ndarray[double, ndim=1, mode="c"] g_density
    g_density = gauss_density(s)
    cdef double constraint = 0.0
    cdef double density = 0.0
    cdef long n = s.N
    cdef long x

    with nogil:
        constraint = 0.0
        density = 0.0
        for x in prange(n):
            constraint += la.algebra_sq(&g_density[3*x])
            density += la.algebra_sq(&rho[3*x])

    print(density, constraint)

    return constraint / density

# Gauss density
def gauss_density(s):
    cdef:
        cnp.ndarray[double, ndim=1, mode="c"]   u0 = s.up
        cnp.ndarray[double, ndim=1, mode="c"]   u1 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"]   u2 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"]   e0 = s.ep
        cnp.ndarray[double, ndim=1, mode="c"]   e1 = s.e
        cnp.ndarray[double, ndim=1, mode="c"]   rho = s.r
        cnp.ndarray[long, ndim=1, mode="c"]     dims = s.dims
        cnp.ndarray[long, ndim=1, mode="c"]     acc = s.acc
        cnp.ndarray[long, ndim=1, mode="c"]     iter_dims = s.iter_dims
        cnp.ndarray[double, ndim=1, mode="c"]   a = s.a

        long nx0, nx1, ny0, ny1, nz0, nz1
        double g = s.g
        double a0 = s.dt
        double factor

        long x, i, j, gl, xl, oi, xsi
        double * buffer0
        double * buffer1
        double * buffer2

        cnp.ndarray[double, ndim=1, mode="c"] g_density = np.zeros(3*s.N, dtype=np.double)

    nx0, nx1, ny0, ny1, nz0, nz1 = iter_dims

    for x in range((nx0+1) * dims[1] * dims[2], (nx1-1) * dims[1] * dims[2]):#, nogil=True):
        buffer0 = <double *> malloc(4 * sizeof(double))
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))
        la.algebra_zero(buffer2)
        la.group_zero(buffer0)
        la.group_zero(buffer1)
        # Standard electric contribution D_i E_x,i - rho
        # ... Using temporal plaquettes
        for i in range(3):
            ccommon.tplaq(&u1[0], &u2[0], x, i, +1, &dims[0], &acc[0], buffer0)
            ccommon.tplaq(&u1[0], &u2[0], x, i, -1, &dims[0], &acc[0], buffer1)
            la.group_add(buffer0, buffer1, 1)
            la.proj(buffer0, buffer1)
            la.algebra_add(&g_density[3*x], buffer1, -a0 / (a0 * a[i]) ** 2 / g)

        # Charge density
        la.algebra_add(&g_density[3*x], &rho[3*x], -1.0)

        """
        if la.algebra_sq(&g_density[3*x]) > 0:
            with gil:
                print("Before correction")
                print(la.algebra_sq(&g_density[3*x]))
        """
        # Correction terms
        for i in range(3):
            for j in range(1, 3):
                factor = - a0 / (2 * a[i] * a[j]) ** 2 / g
                # Four finite differences
                for oi in range(-1, 3, 2):
                    # 1) + U_x+0,+i (Dj2 C_x,0+i)^t
                    # 3) + U_x+0,-i (Dj2 C_x,0-i)^t
                    la.group_zero(buffer1)
                    dj2_c0i_type2(x, i, oi, j, &u1[0], &u2[0], &dims[0], &acc[0], +1, buffer1)
                    ccommon.link(x, i, oi, &dims[0], &acc[0], &xl, &gl)
                    la.mul2(&u2[gl], buffer1, oi, -1, buffer0)
                    la.proj(buffer0, buffer1)
                    la.algebra_add(&g_density[3*x], buffer1, factor)

                    # 2) - (Dj2 C_x-i,0+i)^t U_x-i,+i
                    # 4) - (Dj2 C_x+i,0-i)^t U_x+i,-i
                    la.group_zero(buffer1)
                    xsi = la.shift2(x, i, -oi, &dims[0], &acc[0])
                    dj2_c0i_type2(xsi, i, oi, j, &u1[0], &u2[0], &dims[0], &acc[0], +1, buffer1)
                    ccommon.link(xsi, i, oi, &dims[0], &acc[0], &xl, &gl)
                    la.mul2(buffer1, &u1[gl], -1, +oi, buffer0)
                    la.proj(buffer0, buffer1)
                    la.algebra_add(&g_density[3*x], buffer1, -factor)

        """
        if la.algebra_sq(&g_density[3*x]) > 0:
            with gil:
                print("After correction")
                print(la.algebra_sq(&g_density[3*x]))
        """

        # Write to result vector
        #la.algebra_add(&g_density[3*x], buffer2, 1)

        free(buffer0)
        free(buffer1)
        free(buffer2)

    return g_density

"""
    C functions
"""
cdef void first_correction_eom(long x, long i, long *dims, long *acc, double *a, double dt, double *u0, double *u1, double *u2, double*r) nogil:
    cdef long j, oj, xsj, xsi, xl, gl
    cdef double[4] buff, Dj2buffer
    cdef double[3] rbuff

    la.group_zero(Dj2buffer)
    for j in range(1, 3):
        la.group_zero(buff)
        # four terms
        """
        dj2_c0i_type1(x, i, j, u1, u2, dims, acc, +1, buff) #1
        dj2_c0i_type1(x, i, j, u2, u1, dims, acc, -1, buff) #4
        dj2_c0i_type1(x, i, j, u1, u0, dims, acc, +1, buff) #2
        dj2_c0i_type1(x, i, j, u0, u1, dims, acc, -1, buff) #3
        """
        dj2_c0i_type2(x, i, +1, j, u1, u2, dims, acc, +1, buff) #1
        dj2_c0i_type2(x, i, +1, j, u2, u1, dims, acc, -1, buff) #4
        dj2_c0i_type2(x, i, +1, j, u1, u0, dims, acc, +1, buff) #2
        dj2_c0i_type2(x, i, +1, j, u0, u1, dims, acc, -1, buff) #3

        la.group_add(Dj2buffer, buff, dt / (2.0 * a[j]) ** 2)

    la.mul2(&u1[la.get_group_index(x, i)], Dj2buffer, 1, -1, buff)
    la.proj(buff, rbuff)
    la.algebra_add(r, rbuff, +1)


cdef void second_correction_eom(long x, long i, long *dims, long *acc, double *a, double dt, double *u0, double *u1, double *u2, double*r) nogil:
    cdef long j, oj, o, xsi, xsj
    cdef double[4] CUC, b1, b2, b3
    cdef double[3] rbuff

    xsi = la.shift2(x, i, 1, dims, acc)
    la.group_zero(CUC)
    for j in range(3):
        for oj in range(-1, 3, 2):
            xsj = la.shift2(x, j, oj, dims, acc)
            # C_x+i,0j * U^t_x+0+j,i C^t_x,0j
            c0i_2(xsi, j, oj, u1, u2, dims, acc, b2)
            la.mul2(b2, &u2[la.get_group_index(xsj, i)], 1, -1, b3)
            c0i_2(x, j, oj, u1, u2, dims, acc, b2)
            la.mul2(b3, b2, 1, -1, b1)
            la.group_add(CUC, b1, -dt / (2 * a[j])**2)

            # C_x-0+i,0j U^t_x-0+j,i C^t_x-0,0j
            #c0i_2(xsi, j, oj, u0, u1, dims, acc, b2)
            c0i_2(xsi, j, oj, u1, u0, dims, acc, b2)
            la.mul2(b2, &u0[la.get_group_index(xsj, i)], 1, -1, b3)
            #c0i_2(x, j, oj, u0, u1, dims, acc, b2)
            c0i_2(x, j, oj, u1, u0, dims, acc, b2)
            la.mul2(b3, b2, 1, -1, b1)
            la.group_add(CUC, b1, -dt / (2 * a[j])**2)

    la.mul2(&u1[la.get_group_index(x, i)], CUC, 1, 1, b1)
    la.proj(b1, rbuff)
    la.algebra_add(r, rbuff, +1)

cdef void dj2_c0i_type1(long x, long i, long j, double *u, double *v, long*dims, long*acc, double f, double *result) nogil:
    cdef long oj, xsj, xsi, xl, gl
    cdef double[4] b0, b1
    xsi = la.shift2(x, i, +1, dims, acc)

    for oj in range(-1, 3, 2):
        xsj = la.shift2(x, j, oj, dims, acc)
        la.group_zero(b0)
        la.group_add(b0, &v[la.get_group_index(xsj, i)], +1)
        la.group_add(b0, &u[la.get_group_index(xsj, i)], -1)
        ccommon.link(x, j, oj, dims, acc, &xl, &gl)
        la.mul2(&u[gl], b0, oj, 1, b1)
        ccommon.link(xsi, j, oj, dims, acc, &xl, &gl)
        la.mul2(b1, &v[gl], 1, -oj, b0)
        la.group_add(result, b0, +f)

    la.group_zero(b0)
    la.group_add(b0, &v[la.get_group_index(x, i)], +1)
    la.group_add(b0, &u[la.get_group_index(x, i)], -1)
    la.group_add(result, b0, -2*f)

cdef void dj2_c0i_type2(long x, long i, long oi, long j, double *u, double *v, long*dims, long*acc, double f, double *result) nogil:
    cdef long oj, xsj, xsi, xl, gl
    cdef double[4] b0, b1
    xsi = la.shift2(x, i, oi, dims, acc)

    for oj in range(-1, 3, 2):
        xsj = la.shift2(x, j, oj, dims, acc)
        la.group_zero(b0)
        ccommon.link(xsj, i, oi, dims, acc, &xl, &gl)
        la.group_add(b0, &v[gl], +1)
        la.group_add(b0, &u[gl], -1)
        if oi < 0:
            la.group_hc(b0)

        ccommon.link(x, j, oj, dims, acc, &xl, &gl)
        la.mul2(&u[gl], b0, oj, 1, b1)
        ccommon.link(xsi, j, oj, dims, acc, &xl, &gl)
        la.mul2(b1, &v[gl], 1, -oj, b0)
        la.group_add(result, b0, +f)

    la.group_zero(b0)
    ccommon.link(x, i, oi, dims, acc, &xl, &gl)
    la.group_add(b0, &v[gl], +1)
    la.group_add(b0, &u[gl], -1)
    if oi < 0:
        la.group_hc(b0)
    la.group_add(result, b0, -2*f)


cdef void c0i(long x, long i, double *up, double *uf, long*dims, long*acc, double *result) nogil:
    cdef long gi = la.get_group_index(x, i)
    la.group_zero(result)
    la.group_add(result, &uf[gi], +1)
    la.group_add(result, &up[gi], -1)

cdef void c0i_2(long x, long i, long oi, double *up, double *uf, long*dims, long*acc, double *result) nogil:
    cdef long xi, gi
    ccommon.link(x, i, oi, dims, acc, &xi, &gi)
    la.group_zero(result)
    la.group_add(result, &uf[gi], +1)
    la.group_add(result, &up[gi], -1)
    # manual hermitian conjugate
    if oi < 0:
        la.group_hc(result)

# Dj2 C_x,0i
cdef void Dj2_c0i(long x, long j, long i, double *up, double *uf, long *dims, long *acc, double f, double *result) nogil:
    cdef long oj, gl, xl, xsi
    cdef double[4] b1, b2

    xsi = la.shift2(x, i, 1, dims, acc)
    for oj in range(-1, 3, 2):
        xsj = la.shift2(x, j, oj, dims, acc)
        c0i(xsj, i, up, uf, dims, acc, b1)
        ccommon.link(x, j, oj, dims, acc, &xl, &gl)
        la.mul2(&up[gl], b1, oj, 1, b2)
        ccommon.link(xsi, j, oj, dims, acc, &xl, &gl)
        la.mul2(b2, &uf[gl], 1, -oj, b1)
        la.group_add(result, b1, +f)
    c0i(x, i, up, uf, dims, acc, b1)
    la.group_add(result, b1, -2*f)

# Dj2 C_x,-0i
cdef void Dj2_cm0i(long x, long j, long i, double *up, double *uf, long *dims, long *acc, double f, double *result) nogil:
    cdef long oj, gl, xl, xsi
    cdef double[4] b1, b2

    xsi = la.shift2(x, i, 1, dims, acc)
    for oj in range(-1, 3, 2):
        xsj = la.shift2(x, j, oj, dims, acc)
        c0i(xsj, i, up, uf, dims, acc, b1)
        ccommon.link(x, j, oj, dims, acc, &xl, &gl)
        la.mul2(&uf[gl], b1, oj, 1, b2)
        ccommon.link(xsi, j, oj, dims, acc, &xl, &gl)
        la.mul2(b2, &up[gl], 1, -oj, b1)
        la.group_add(result, b1, +f)
    c0i(x, i, up, uf, dims, acc, b1)
    la.group_add(result, b1 , -2*f)

# Dj2 C_x,0-i
cdef void Dj2_c0mi(long x, long j, long i, double *up, double *uf, long *dims, long *acc, double f, double *result) nogil:
    cdef long oj, gl, xl, xsi
    cdef double[4] b1, b2

    xsi = la.shift2(x, i, -1, dims, acc)
    for oj in range(-1, 3, 2):
        xsj = la.shift2(x, j, oj, dims, acc)
        c0i_2(xsj, i, -1, up, uf, dims, acc, b1)
        ccommon.link(x, j, oj, dims, acc, &xl, &gl)
        la.mul2(&up[gl], b1, oj, 1, b2)
        ccommon.link(xsi, j, oj, dims, acc, &xl, &gl)
        la.mul2(b2, &uf[gl], 1, -oj, b1)
        la.group_add(result, b1, +f)
    c0i_2(x, i, -1, up, uf, dims, acc, b1)
    la.group_add(result, b1, -2*f)

# compute staple sum for optimized eom
cdef void compute_staple_sum(long x, long d, double*links, long*dims, long*acc, double*plaq_factor,
                             double *result) nogil:
    cdef double buffer1[4]
    cdef double buffer2[4]
    cdef double buffer_S[4]
    cdef long i, ci1, ci2, ci3, ci4
    ci1 = la.shift2(x, d, 1, dims, acc)
    la.group_zero(&buffer_S[0])
    for i in range(3):
        if i != d:
            ci2 = la.shift2(x, i, 1, dims, acc)
            ci3 = la.shift2(ci1, i, -1, dims, acc)
            ci4 = la.shift2(x, i, -1, dims, acc)
            la.mul2(&links[4 * (3 * ci1 + i)], &links[4 * (3 * ci2 + d)], 1, -1, &buffer1[0])
            la.mul2(&buffer1[0], &links[4 * (3 * x + i)], 1, -1, &buffer2[0])
            la.group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
            la.mul2(&links[4 * (3 * ci3 + i)], &links[4 * (3 * ci4 + d)], -1, -1, &buffer1[0])
            la.mul2_fast(&buffer1[0], &links[4 * (3 * ci4 + i)], &buffer2[0])
            la.group_add(&buffer_S[0], &buffer2[0], plaq_factor[i])
    la.mul2_fast(&links[4 * (3 * x + d)], &buffer_S[0], &buffer1[0])
    la.proj(&buffer1[0], &result[0])