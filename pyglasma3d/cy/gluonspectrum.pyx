#cython: boundscheck=False, wraparound=False, nonecheck=False
"""
    Coulomb gauge
"""
from lattice cimport *
from pyglasma3d.core import Simulation

cimport cython
import cython
import numpy as np
import coulomb
import scipy.fftpack
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

cdef double PI = np.pi

"""
    Computes the gluon spectrum (i.e. occupation numbers in momentum space) as in arXiv:1610.03711.
"""
def compute_occupation_numbers1(s):

    cdef:
        cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
        cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1
        cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
        cnp.ndarray[double, ndim=1, mode="c"] current = s.j
        cnp.ndarray[double, ndim=1, mode="c"] spacings = s.a
        cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
        long ix, iy, iz, nx, ny, nz, x, d, gi, c, fi
        double* pbuffer1
        double* pbuffer2
        double[3] factor

    nx, ny, nz = s.dims
    n = nx * ny * nz

    for d in range(3):
        factor[d] = 1.0 / (s.g * s.a[d])

    # apply coulomb gauge
    coulomb.apply_coulomb_gauge(s)

    # extract (time-averaged) gauge field from links and apply FFT
    cdef cnp.ndarray[double, ndim=1, mode="c"] gf_re = np.zeros(nx * ny * nz * 3 * 3, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] gf_im = np.zeros(nx * ny * nz * 3 * 3, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] gf = np.zeros(nx * ny * nz * 3 * 3, dtype=np.double)
    get_gauge_field(&links0[0], &links1[0], &factor[0], &dims[0], &gf[0])
    gf_fft = np.fft.fftn(np.reshape(gf, (nx, ny, nz, 3, 3)), axes=[0, 1, 2]).flatten()
    gf_re = np.real(gf_fft).copy()
    gf_im = np.imag(gf_fft).copy()

    # extract electric field and apply FFT
    cdef cnp.ndarray[double, ndim=1, mode="c"] el = np.zeros(nx * ny * nz * 3 * 3, dtype=np.double)
    get_electric_field(&electric[0], &factor[0], &dims[0], &el[0])
    el = np.abs(np.fft.fftn(np.reshape(el, (nx, ny, nz, 3, 3)), axes=[0, 1, 2]).flatten())

    # extract current density and apply FFT
    cdef cnp.ndarray[double, ndim=1, mode="c"] j_re = np.zeros(nx * ny * nz * 3, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] j_im = np.zeros(nx * ny * nz * 3, dtype=np.double)
    j_fft = np.fft.fftn(np.reshape(current, (nx, ny, nz, 3)), axes=[0, 1, 2]).flatten()
    j_re = np.real(j_fft).copy()
    j_im = np.imag(j_fft).copy()

    # compute occupation numbers
    cdef double omega, pfactor
    pfactor = 1.0 / (16.0 * np.pi ** 3)
    cdef cnp.ndarray[double, ndim=3, mode="c"] occupation = np.zeros((nx, ny, nz), dtype=np.double)
    for ix in prange(nx, nogil=True, schedule="guided"):
        for iy in range(ny):
            for iz in range(nz):
                x = get_index_nm(ix, iy, iz, &dims[0])
                if x > 0:
                    omega = sqrt(k2_latt(ix, iy, iz, &dims[0], &spacings[0]))
                    for d in range(3):
                        fi = get_field_index(x, d)
                        for c in range(3):
                            occupation[ix, iy, iz] += 0.5 * el[fi+c] ** 2
                            occupation[ix, iy, iz] += 0.5 * (gf_re[fi+c] ** 2 + gf_im[fi+c] ** 2) * omega ** 2
                    # add current term
                    for c in range(3):
                        occupation[ix, iy, iz] -= (j_re[3*x+c] * gf_re[3*3*x+c] + j_im[3*x+c] * gf_im[3*3*x+c])

    return occupation * pfactor


cdef inline double k2_latt(long ix, long iy, long iz, long* dims, double* a) nogil:
    cdef double result = 0.0
    result += 2.0 * (1.0 - cos((2.0 * PI * ix) / dims[0])) / (a[0] * a[0])
    result += 2.0 * (1.0 - cos((2.0 * PI * iy) / dims[1])) / (a[1] * a[1])
    result += 2.0 * (1.0 - cos((2.0 * PI * iz) / dims[2])) / (a[2] * a[2])
    return result


cdef void get_gauge_field(double* u0, double* u1, double* factor, long* dims, double* gf):
    cdef:
        long x, d, gi, fi, n, c
        double* pbuffer1
        double* pbuffer2

    n = dims[0] * dims[1] * dims[2]
    for x in prange(n, nogil=True, schedule="guided"):
        pbuffer1 = <double *>malloc(3 * sizeof(double))
        pbuffer2 = <double *>malloc(3 * sizeof(double))

        for d in range(3):
            gi = get_group_index(x, d)
            fi = get_field_index(x, d)
            algebra_zero(pbuffer2)
            mlog(&u0[gi], pbuffer1)
            algebra_add(pbuffer2, pbuffer1, 0.5 * factor[d])
            mlog(&u1[gi], pbuffer1)
            algebra_add(pbuffer2, pbuffer1, 0.5 * factor[d])

            for c in range(3):
                gf[fi+c] = pbuffer2[c]

cdef void get_electric_field(double* electric, double* factor, long* dims, double* el):
    cdef:
        long x, d, fi, n, c

    n = dims[0] * dims[1] * dims[2]
    for x in prange(n, nogil=True, schedule="guided"):
        for d in range(3):
            fi = get_field_index(x, d)
            for c in range(3):
                el[fi+c] = electric[fi+c] * factor[d]