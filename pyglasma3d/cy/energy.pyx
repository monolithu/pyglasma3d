#cython: boundscheck=False, wraparound=False, nonecheck=False
"""
    A module for computing various components of the energy momentum tensor, field components etc..
"""
from lattice cimport *

cimport cython
import cython
import numpy as np
cimport numpy as cnp
from libc.math cimport sin, cos, exp, sqrt
from cython.parallel cimport prange, parallel
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

# squared electric field component
cdef inline double electric_sq(double* electric, long x, long d) nogil:
    cdef:
        double result = 0.0
        long field_index

    field_index = get_field_index(x, d)
    return algebra_sq(&electric[field_index])

# squared magnetic field component
cdef inline double magnetic_sq(double* links, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer[4]
        double proj_buffer[3]

    plaq_pos(links, x, d1, d2, dims, acc, plaq_buffer)
    proj(plaq_buffer, proj_buffer)

    return algebra_sq(&proj_buffer[0])

# squared magnetic field component (second variant)
cdef inline double magnetic_sq2(double* links, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer[4]

    plaq_pos(links, x, d1, d2, dims, acc, plaq_buffer)

    return (1.0 - plaq_buffer[0]) * 8.0

# squared magnetic field component (third variant)
cdef inline double magnetic_sq3(double* links0, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer0[4]
        double plaq_buffer1[4]
        double proj_buffer0[3]
        double proj_buffer1[3]
        double result = 0.0

    plaq_pos(links0, x, d1, d2, dims, acc, plaq_buffer0)
    proj(plaq_buffer0, proj_buffer0)

    plaq_pos(links1, x, d1, d2, dims, acc, plaq_buffer1)
    proj(plaq_buffer1, proj_buffer1)

    return algebra_dot(proj_buffer0, proj_buffer1)

# squared magnetic field component (fourth variant)
cdef inline double magnetic_sq4(double* links0, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer0[4]
        double plaq_buffer1[4]
        double proj_buffer0[3]
        double proj_buffer1[3]
        double result = 0.0

    plaq_pos(links0, x, d1, d2, dims, acc, plaq_buffer0)
    proj(plaq_buffer0, proj_buffer0)

    plaq_pos(links1, x, d1, d2, dims, acc, plaq_buffer1)
    proj(plaq_buffer1, proj_buffer1)

    algebra_add(proj_buffer0, proj_buffer1, 1.0)

    return 0.25 * algebra_sq(proj_buffer0)

# squared magnetic field component (fifth variant)
cdef inline double magnetic_sq5(double* links0, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer0[4]
        double plaq_buffer1[4]
        double proj_buffer0[3]
        double proj_buffer1[3]
        double result = 0.0

    plaq_pos(links0, x, d1, d2, dims, acc, plaq_buffer0)
    proj(plaq_buffer0, proj_buffer0)

    plaq_pos(links1, x, d1, d2, dims, acc, plaq_buffer1)
    proj(plaq_buffer1, proj_buffer1)

    #algebra_add(proj_buffer0, proj_buffer1, 1.0)

    return 0.5 * (algebra_sq(proj_buffer0) + algebra_sq(proj_buffer1))

# squared magnetic field component (fifth variant)
cdef inline double magnetic_sq6(double* links0, double* links1, long x, long d, long* dims, long* acc) nogil:
    cdef:
        long d1 = mod(d + 1, 3)
        long d2 = mod(d + 2, 3)
        double plaq_buffer0[4]
        double plaq_buffer1[4]
        double proj_buffer0[3]
        double proj_buffer1[3]
        double result = 0.0

    plaq_pos(links0, x, d1, d2, dims, acc, plaq_buffer0)

    plaq_pos(links1, x, d1, d2, dims, acc, plaq_buffer0)
    #algebra_add(proj_buffer0, proj_buffer1, 1.0)

    return 0.5 * ((1.0 - plaq_buffer0[0]) * 8.0 + (1.0 - plaq_buffer0[0]) * 8.0)

# naive way to compute poynting vector
cdef inline double poynting1(double* links0, double* links1, double* electric, long x, long* dims, long* acc) nogil:
    cdef:
        double result = 0.0
        double buffer0[4]
        double buffer1[3]
        long f_i

    # S_L = E_y B_z - E_z B_y
    # no spatial, only temporal averaging

    # E_y * B_z
    f_i = get_field_index(x, 1)
    plaq_pos(links0, x, 0, 1, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)
    plaq_pos(links1, x, 0, 1, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)

    # E_z * B_y
    f_i = get_field_index(x, 2)
    plaq_pos(links0, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)
    plaq_pos(links1, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)

    return 0.5 * result

# Poynting vector on Yee-grid
cdef inline double poynting2(double* links0, double* links1, double* electric, long x, long* dims, long* acc) nogil:
    cdef:
        double result = 0.0
        double buffer0[4]
        double buffer1[3]
        long f_i1, f_i2, xs

    # S_L = E_y B_z - E_z B_y
    # spatial (for E) and temporal (for B) averaging
    # end result is defined in-between lattice sites

    # E_y * B_z
    xs = shift2(x, 0, 1, dims, acc)
    f_i1 = get_field_index(x, 1)
    f_i2 = get_field_index(xs, 1)
    plaq_pos(links0, x, 0, 1, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i1], buffer1)
    result += algebra_dot(&electric[f_i2], buffer1)
    plaq_pos(links1, x, 0, 1, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i1], buffer1)
    result += algebra_dot(&electric[f_i2], buffer1)

    # E_z * B_y
    f_i1 = get_field_index(x, 2)
    f_i2 = get_field_index(xs, 2)
    plaq_pos(links0, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i1], buffer1)
    result -= algebra_dot(&electric[f_i2], buffer1)
    plaq_pos(links1, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i1], buffer1)
    result -= algebra_dot(&electric[f_i2], buffer1)

    return 0.25 * result

# Poynting vector on Yee-grid (different way to average B)
cdef inline double poynting3(double* links0, double* links1, double* electric, long x, long* dims, long* acc) nogil:
    cdef:
        double result = 0.0
        double buffer0[4]
        double buffer1[3]
        long f_i

    # S_L = E_y B_z - E_z B_y
    # spatial and temporal (for B) averaging
    # end result is defined on lattice sites

    # E_y * B_z
    f_i = get_field_index(x, 1)
    plaq_pos(links0, x, 1, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)

    plaq_neg(links0, x, 1, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)

    plaq_pos(links1, x, 1, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)

    plaq_neg(links1, x, 1, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)

    # E_z * B_y
    f_i = get_field_index(x, 2)
    plaq_pos(links0, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)

    plaq_neg(links0, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)

    f_i = get_field_index(x, 2)
    plaq_pos(links1, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result -= algebra_dot(&electric[f_i], buffer1)

    plaq_neg(links1, x, 2, 0, dims, acc, buffer0)
    proj(buffer0, buffer1)
    result += algebra_dot(&electric[f_i], buffer1)

    return 0.25 * result


# relevant energy density components, averaged over transverse plane
def energy_momentum_tensor(s):
    cdef long x, d, ix, iy, iz
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = s.iter_dims
    cdef long nx = s.dims[0]-2
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1

    cdef cnp.ndarray[double, ndim=1, mode="c"] EL = np.zeros(nx, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] BL = np.zeros(nx, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] ET = np.zeros(nx, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] BT = np.zeros(nx, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] SL = np.zeros(nx, dtype=np.double)

    for ix in prange(1, nx+1, nogil=True):
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                EL[ix-1] += electric_sq(&electric[0], x, 0)
                BL[ix-1] += magnetic_sq5(&links0[0], &links1[0], x, 0, &dims[0], &acc[0])
                SL[ix-1] += poynting3(&links0[0], &links1[0], &electric[0], x, &dims[0], &acc[0])
                for d in range(1, 3):
                    ET[ix-1] += electric_sq(&electric[0], x, d)
                    BT[ix-1] += magnetic_sq5(&links0[0], &links1[0], x, d, &dims[0], &acc[0])


    cdef double plane_avg_factor = 1.0 / (dims[1] * dims[2])
    cdef double g2 = s.g ** 2
    cdef double aL = s.a[0]
    cdef double aT = s.a[1]

    cdef double EL_factor = 0.5 / (g2 * aL ** 2)
    cdef double BL_factor = 0.5 / (g2 * aT ** 4)
    cdef double ET_factor = 0.5 / (g2 * aT ** 2)
    cdef double BT_factor = 0.5 / (g2 * (aL * aT) ** 2)
    cdef double SL_factor = 1.0 / (g2 * aT * aT * aL)

    EL *= plane_avg_factor * EL_factor
    BL *= plane_avg_factor * BL_factor
    ET *= plane_avg_factor * ET_factor
    BT *= plane_avg_factor * BT_factor
    SL *= plane_avg_factor * SL_factor

    return EL, shift_array(BL), shift_array(ET), BT, shift_array(SL)

def shift_array(a):
    return (a + np.roll(a, -1)) / 2.0

def energy_3d(s):
    cdef long x, d, ix, iy, iz
    cdef long nx0, nx1, ny0, ny1, nz0, nz1
    nx0, nx1, ny0, ny1, nz0, nz1 = s.iter_dims
    cdef long nx = s.dims[0]
    cdef long ny = s.dims[1]
    cdef long nz = s.dims[2]
    cdef cnp.ndarray[long, ndim=1, mode="c"] dims = s.dims
    cdef cnp.ndarray[long, ndim=1, mode="c"] acc = s.acc
    cdef cnp.ndarray[double, ndim=1, mode="c"] a = s.a
    cdef cnp.ndarray[double, ndim=1, mode="c"] electric = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] links0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] links1 = s.u1

    cdef cnp.ndarray[double, ndim=3, mode="c"] e = np.zeros((nx, ny, nz), dtype=np.double)

    cdef double[3] b_factor
    b_factor[0] = a[1] * a[2]
    b_factor[1] = a[0] * a[2]
    b_factor[2] = a[0] * a[1]

    for ix in prange(nx0, nx1, nogil=True):
        for iy in range(ny0, ny1):
            for iz in range(nz0, nz1):
                x = get_index_nm(ix, iy, iz, &dims[0])
                for d in range(3):
                    e[ix, iy, iz] += electric_sq(&electric[0], x, d) * 0.5 / a[d] ** 2
                    #e[ix, iy, iz] += magnetic_sq5(&links0[0], &links1[0], x, d, &dims[0], &acc[0]) * 0.5 / b_factor[d]
    return e
