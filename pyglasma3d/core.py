from __future__ import print_function

import numpy as np

import pyglasma3d.cy.solver_leapfrog            as leapfrog
import pyglasma3d.cy.solver_leapfrog2           as leapfrog2
import pyglasma3d.cy.solver_semiimplicit_early  as semiimplicit_early
import pyglasma3d.cy.solver_implicit            as implicit
import pyglasma3d.cy.solver_semiimplicit        as semiimplicit
import pyglasma3d.cy.solver_semiimplicit2       as semiimplicit2
import pyglasma3d.cy.solver_implicit_common     as common

import pyglasma3d.cy.gauss as gauss
import pyglasma3d.cy.interpolate as interpolate
import pyglasma3d.cy.energy as energy

from time import time

"""
    Simulation class used for the leapfrog solver
"""


class Simulation:
    def __init__(self, dims, iter_dims, a, dt, g):
        self.dims = np.array(dims, dtype=np.int64)
        self.iter_dims = np.array(iter_dims, dtype=np.int64)
        self.a = np.array(a, dtype=np.double)
        self.dt = np.array([dt], dtype=np.double)[0]
        self.g = np.array([g], dtype=np.double)[0]
        self.acc = np.array([
            dims[0] * dims[1] * dims[2],
            dims[1] * dims[2],
            dims[2],
            1
        ], dtype=np.int64)

        self.N = dims[0] * dims[1] * dims[2]
        self.NL = dims[0]
        self.NT = dims[1]

        self.u0 = np.empty(self.N * 3 * 4, dtype=np.double)
        self.u1 = np.empty(self.N * 3 * 4, dtype=np.double)
        self.e = np.empty(self.N * 3 * 3, dtype=np.double)
        self.j = np.empty(self.N * 3, dtype=np.double)
        self.r = np.empty(self.N * 3, dtype=np.double)
        self.staple_neighbours = np.empty(self.N * 3 * 7, dtype=np.int64)
        leapfrog.compute_staple_neighbours(self)

        self.charges = []
        self.t = 0

        self.write_counter = 0
        self.debug = False

        # this is used for checks in mv, interpolate to decide if we are running the old leapfrog code
        # or a new implicit scheme
        self.mode = None

        self.reset()

    def init(self):
        interpolate.interpolate_to_charge_density(self)
        leapfrog.evolve_u(self)

    def reset(self):
        self.u0[:] = 0.0
        self.u1[:] = 0.0
        self.e[:] = 0.0
        self.j[:] = 0.0
        self.r[:] = 0.0

        self.u0[0:-1:4] = 1.0
        self.u1[0:-1:4] = 1.0

    def reset_charges_and_currents(self):
        self.j[:] = 0.0
        self.r[:] = 0.0

    def swap(self):
        tmp = self.u1
        self.u1 = self.u0
        self.u0 = tmp

    def evolve(self):
        interpolate.advance_and_interpolate_currents(self)
        interpolate.interpolate_to_charge_density(self)
        self.swap()
        leapfrog.evolve(self)
        self.t += self.dt

    def get_nbytes(self):
        grid_nbytes = self.u0.nbytes + self.u1.nbytes + self.e.nbytes + self.j.nbytes + self.r.nbytes
        grid_nbytes += self.staple_neighbours.nbytes
        charges_nbytes = sum([c.get_nbytes() for c in self.charges])
        return grid_nbytes + charges_nbytes

    def log(self, *msg):
        if self.debug:
            outputstr = "[t={0}]: ".format(self.t)
            for m in msg:
                outputstr += str(m) + " "
            print(outputstr)

    def write_energy_momentum(self, max_writes, file_path):
        EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(self)
        nx = self.NL - 2

        # data format:
        # header: NL-2, max_t
        # body: BL, BL, ET, BT, SL
        def toint(i):
            return np.array([i], dtype=np.int64).tobytes()

        # open file
        import os
        if not os.path.isfile(file_path):
            dirname = os.path.dirname(file_path)
            if not os.path.exists(dirname):
                os.makedirs(os.path.dirname(file_path))
        # write header
        if self.write_counter == 0:
            f = open(file_path, mode='w+')
            f.write(toint(nx))
            f.write(toint(max_writes))
        elif self.write_counter < max_writes:
            f = open(file_path, mode='a+')
        else:
            f = None

        if f is not None:
            # write body
            f.write(EL.tobytes())
            f.write(BL.tobytes())
            f.write(ET.tobytes())
            f.write(BT.tobytes())
            f.write(SL.tobytes())
            f.close()
            self.write_counter += 1

        return

    def gauss_constraint(self):
        return gauss.gauss_sq_total(self)


class ChargePlane:
    def __init__(self, dims, index_offset, subcell_shift, orientation):
        self.N = dims[1] * dims[2]
        self.index_offset = index_offset
        self.subcell_shift = subcell_shift
        self.orientation = orientation
        self.q = np.zeros(self.N * 3, dtype=np.double)
        # charge planes become inactive once they leave the simulation box. they aren't removed automatically.
        self.active = True

    def get_nbytes(self):
        return self.q.nbytes

"""
    Observables
"""


class Observables:
    @staticmethod
    def energy_momentum_tensor(s):
        return energy.energy_momentum_tensor(s)

    @staticmethod
    def gauss_constraint_squared(s):
        return gauss.gauss_sq_total(s)


"""
    Simulation class used for the implicit solver
"""


class SimulationImplicit:
    def __init__(self, dims, iter_dims, a, dt, g):
        self.dims = np.array(dims, dtype=np.int64)
        self.iter_dims = np.array(iter_dims, dtype=np.int64)
        self.a = np.array(a, dtype=np.double)
        self.dt = np.array([dt], dtype=np.double)[0]
        self.g = np.array([g], dtype=np.double)[0]
        self.acc = np.array([
            dims[0] * dims[1] * dims[2],
            dims[1] * dims[2],
            dims[2],
            1
        ], dtype=np.int64)

        self.N = dims[0] * dims[1] * dims[2]
        self.NL = dims[0]
        self.NT = dims[1]

        self.up = np.empty(self.N * 3 * 4, dtype=np.double) # past links
        self.u0 = np.empty(self.N * 3 * 4, dtype=np.double) # present links
        self.u1 = np.empty(self.N * 3 * 4, dtype=np.double) # future links
        self.ep = np.empty(self.N * 3 * 3, dtype=np.double) # past electric field
        self.e = np.empty(self.N * 3 * 3, dtype=np.double)  # future electric field
        self.j = np.empty(self.N * 3, dtype=np.double)
        self.r = np.empty(self.N * 3, dtype=np.double)

        # staple neighbours are the same is in the leapfrog case
        self.staple_neighbours = np.empty(self.N * 3 * 7, dtype=np.int64)
        leapfrog.compute_staple_neighbours(self)

        self.charges = []
        self.t = 0

        self.write_counter = 0
        self.debug = False

        self.reset()

        # parameters for implicit solver (override if needed)
        self.iterations = 20
        self.damping = 0.3

        """
            Implicit modes:
            0 ... Isotropic, Implicit
            1 ... Dispersion-free Semi-Implicit
            2 ... (Early, kinda broken) Semi-Implicit
            3 ... Simple Leapfrog (slower, for debugging)
            4 ... Consistent Dispersion-free Semi-Implicit
        """
        self.mode = 0

    def init(self):
        interpolate.interpolate_to_charge_density(self)
        common.evolve_links2(self)
        #leapfrog.evolve_u(self)
        self.ep = self.e.copy()
        self.up = self.u0.copy()

    def reset(self):
        self.up[:] = 0.0
        self.u0[:] = 0.0
        self.u1[:] = 0.0
        self.ep[:] = 0.0
        self.e[:] = 0.0
        self.j[:] = 0.0
        self.r[:] = 0.0

        self.up[0:-1:4] = 1.0
        self.u0[0:-1:4] = 1.0
        self.u1[0:-1:4] = 1.0

    def reset_charges_and_currents(self):
        self.j[:] = 0.0
        self.r[:] = 0.0

    def swap(self):
        self.up, self.u0, self.u1 = self.u0, self.u1, self.up
        self.ep, self.e = self.e, self.ep

    def evolve_initial(self):
        self.swap()
        # interpolation is the same as in the leapfrog solver
        interpolate.advance_and_interpolate_currents(self)
        interpolate.interpolate_to_charge_density(self)

        if self.mode == 0:
            implicit.evolve_initial(self)
        elif self.mode == 1:
            semiimplicit.evolve_initial(self)
        elif self.mode == 2:
            semiimplicit_early.evolve_initial(self)
        elif self.mode == 3:
            leapfrog2.evolve_initial(self)
        elif self.mode == 4:
            semiimplicit2.evolve_initial(self)
        else:
            print("Unknown solver mode.")
        self.t += self.dt

    def evolve_iterate(self):
        if self.mode == 0:
            implicit.evolve_iterate(self, self.iterations, self.damping)
        elif self.mode == 1:
            semiimplicit.evolve_iterate(self, self.iterations, self.damping)
        elif self.mode == 2:
            semiimplicit_early.evolve_iterate(self, self.iterations, self.damping)
        elif self.mode == 3:
            pass
        elif self.mode == 4:
            semiimplicit2.evolve_iterate(self, self.iterations, self.damping)
        else:
            print("Unknown solver mode.")

    def evolve(self):
        self.evolve_initial()
        self.evolve_iterate()

    def get_nbytes(self):
        grid_nbytes = self.u0.nbytes + self.u1.nbytes + self.e.nbytes + self.j.nbytes + self.r.nbytes
        grid_nbytes += self.up.nbytes + self.ep.nbytes
        grid_nbytes += self.staple_neighbours.nbytes
        charges_nbytes = sum([c.get_nbytes() for c in self.charges])
        return grid_nbytes + charges_nbytes

    def log(self, *msg):
        if self.debug:
            outputstr = "[t={0}]: ".format(self.t)
            for m in msg:
                outputstr += str(m) + " "
            print(outputstr)

    def write_energy_momentum(self, max_writes, file_path):
        # TODO
        EL, BL, ET, BT, SL = Observables.energy_momentum_tensor(self)
        nx = self.NL - 2

        # data format:
        # header: NL-2, max_t
        # body: BL, BL, ET, BT, SL
        def toint(i):
            return np.array([i], dtype=np.int64).tobytes()

        # open file
        import os
        if not os.path.isfile(file_path):
            dirname = os.path.dirname(file_path)
            if not os.path.exists(dirname):
                os.makedirs(os.path.dirname(file_path))
        # write header
        if self.write_counter == 0:
            f = open(file_path, mode='w+')
            f.write(toint(nx))
            f.write(toint(max_writes))
        elif self.write_counter < max_writes:
            f = open(file_path, mode='a+')
        else:
            f = None

        if f is not None:
            # write body
            f.write(EL.tobytes())
            f.write(BL.tobytes())
            f.write(ET.tobytes())
            f.write(BT.tobytes())
            f.write(SL.tobytes())
            f.close()
            self.write_counter += 1

        return

    def gauss_constraint(self):
        if self.mode == 0:
            return implicit.gauss_constraint(self)
        elif self.mode == 1:
            return semiimplicit.gauss_constraint(self)
        elif self.mode == 2:
            return leapfrog2.gauss_constraint(self)
        elif self.mode == 3:
            return leapfrog2.gauss_constraint(self)
        elif self.mode == 4:
            return semiimplicit2.gauss_constraint(self)
        else:
            print("Unknown solver mode.")

    def gauss_density(self):
        if self.mode == 0:
            return implicit.gauss_density(self)
        elif self.mode == 1:
            return semiimplicit.gauss_density(self)
        elif self.mode == 2:
            return leapfrog2.gauss_density(self)
        elif self.mode == 3:
            return leapfrog2.gauss_density(self)
        elif self.mode == 4:
            return semiimplicit2.gauss_density(self)
        else:
            print("Unknown solver mode.")

