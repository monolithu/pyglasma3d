# pyglasma3d
### A 3D Glasma simulation implemented in Cython [![build status](https://gitlab.com/monolithu/pyglasma3d/badges/master/build.svg)](https://gitlab.com/monolithu/pyglasma3d/commits/master)


This project aims to port and optimise the most important features
of the Java-based [OpenPixi](https://github.com/openpixi/openpixi/)
project to [Cython](http://cython.org/) and Python.
The code is used to simulate the early stages of heavy-ion
collisions in the Color Glass Condensate (CGC) framework using the
Colored Particle-in-Cell method (CPIC). The numerical method is
described in detail in our publications: 
[arXiv:1605.07184](https://arxiv.org/abs/1605.07184), 
[arXiv:1703.00017](https://arxiv.org/abs/1703.00017).

Run `setup.sh` to compile the Cython modules run example codes in `pyglasma3d.examples` to try it out, e.g

```python -m pyglasma3d.examples.mv```

**Implemented features**
* Leap-frog solver for Yang-Mills fields on a lattice and
nearest-grid-point (NGP) interpolation for color currents
* Computing various energy momentum tensor components
* Unit tests for many functions
* McLerran Venugopalan model initial conditions with and without
randomness in the longitudinal extent
* Multi-threading using Cython's `prange` (basically OpenMP)
* Distributed memory (MPI) using [mpi4py](http://pythonhosted.org/mpi4py/)
* Coulomb gauge
* Gluon occupation numbers *(inaccurate, still work in progress)*
* Semi-implicit solver for increased stability

**Future features**
* Readable, modular code *(haha)*
* JIMWLK evolution code

**Dependencies**
* Python 2.7.9
* NumPy 1.11.0
* SciPy 0.17.0 (only for unit tests)
* Cython 0.24.1
* gcc 5.4.0 or equivalent C compiler
* Matplotlib 1.5.1 (only for interactive runs)